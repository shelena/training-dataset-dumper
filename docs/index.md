The training dataset dumper is a tool which processes xAOD derivations and provides h5 ntuples as output format.
These files are used in the FTAG group for the training and performance evaluation of algorithms and dedicated studies.

Note that there are **many** different ways to use this code, please skim the basic and advanced usage sections before deciding how to get started.

Getting the code
----------------

The training dataset dumper is hosted on gitlab:

- [https://gitlab.cern.ch/atlas-flavor-tagging-tools/training-dataset-dumper](https://gitlab.cern.ch/atlas-flavor-tagging-tools/training-dataset-dumper)

Docker images are available on CERN GitLab container registry:

- [CERN GitLab container registry](https://gitlab.cern.ch/atlas-flavor-tagging-tools/training-dataset-dumper/container_registry)

Getting Help
------------

If you have questions that aren't covered here, please post them [on AtlasTalk][at], we'll try to respond as quickly as possible.

For more interactive help, consider [joining][mmj] the [ATLAS Flavor Tagging mattermost team][mm]

[at]: https://atlas-talk.web.cern.ch/c/ftag
[mm]: https://mattermost.web.cern.ch/aft-algs/channels/h5-dumper
[mmj]: https://mattermost.web.cern.ch/signup_user_complete/?id=ektad7hj4fdf5nehdmh1js4zfy
