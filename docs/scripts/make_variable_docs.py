#!/usr/bin/env python3

"""
Generate documentation for a given HDF5 file
"""
_spec_help = 'Yaml variable specification file'

from argparse import ArgumentParser
from sys import stdout, stderr
from os.path import join

RESERVED = {'description', 'old_name', 'rank'}
CATEGORIES = {'jets': 'Jet variables', 'tracks': 'Track variables'}


def get_args():
    parser = ArgumentParser(description=__doc__)
    parser.add_argument('h5_file')
    parser.add_argument('-s', '--spec-file', default=join('data', 'field_descriptions.yml'),
                        help=_spec_help)
    output = parser.add_mutually_exclusive_group(required=False)
    output.add_argument('-m', '--markdown-output', action='store_true')
    output.add_argument('-y', '--yaml-output', action='store_true')
    parser.add_argument('-p', '--preamble', default=join('data', 'pflow_preamble.md'))
    parser.add_argument('-e', '--epilogue', default=join('data', 'pflow_epilogue.md'))
    parser.add_argument('-o', '--output', nargs='?', const='vars_pflow.md')
    return parser.parse_args()

def run():
    args = get_args()

    from h5py import File
    import yaml

    with File(args.h5_file, 'r') as h5:
        fields = get_h5_fields(h5)

    with open(args.spec_file) as spec_file:
        spec = yaml.safe_load(spec_file)
        add_meta(fields, spec)

    if args.yaml_output:
        stdout.write(yaml.dump(fields, default_flow_style=False))
    else:
        if not args.output:
            out_file = stdout
        else:
            out_file = open(args.output, 'w')

        with open(args.preamble) as pre:
            for line in pre:
                out_file.write(line)
        write_markdown(fields, out_file)
        with open(args.epilogue) as epe:
            for line in epe:
                out_file.write(line)

ALWAYS_SHOW = {'pt', 'eta', 'mass'}

def rank_key(item):
    key, fields = item
    if 'rank' in fields:
        return (fields['rank'], key)
    else:
        return (1, key)


def write_markdown(fields, outfile, depth=0, ignore=set()):
    sep = '-'
    ignored = set()
    sorted_fields = sorted(fields.items(), key=rank_key)
    for name, sub in sorted_fields:
        if 'description' in sub:
            desc = ' '.join(sub['description'].split())

            print_conditions = [
                name not in ignore,
                sub.get('always_show'),
                name in ALWAYS_SHOW]

            if any(print_conditions):
                outfile.write(f'{sep:>{depth}} **{name}:** {desc}\n')
                ignore.add(name)
            else:
                ignored.add(name)

        else:
            if name in CATEGORIES.keys():
                outfile.write(f'### {CATEGORIES[name]}\n\n')
            else:
                outfile.write(f'{sep:>{depth}} **{name}:**\n')
            write_markdown(sub, outfile, depth+2, ignore)

    if ignored:
        n_ignore = len(ignored)
        if n_ignore > 4:
            vstr = f'{n_ignore}'
        else:
            vstr = ', '.join(sorted(ignored))
        outfile.write(f'{sep:>{depth}} '
                      f'**Previously listed variables:** ({vstr})\n')
    outfile.write('\n')

def add_meta(fields, spec):
    new_fields = {}
    for n, old in fields.items():
        if n not in spec:
            stderr.write(f'{n} not found in {spec}, using default\n')
        elif isinstance(old, str) and not old:
            new_fields[n] = ' '.join(spec[n].split())
        else:
            add_meta(old, spec[n])

    for n, new in new_fields.items():
        fields[n] = new

    for meta in ['rank', 'always_show']:
        if meta in spec:
            fields[meta] = spec[meta]

def get_h5_fields(h5):
    try:
        fields = {}
        for n in h5.dtype.names:
            attribs = {'description': ''}
            fields[n] = attribs
        return fields
    except AttributeError as err:
        if "no attribute 'dtype'" not in err.args[0]:
            raise

    return {s.name.split('/')[-1]: get_h5_fields(s) for s in h5.values()}

if __name__ == '__main__':
    run()
