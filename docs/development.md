### Contributing guidelines

If you want to contribute to the development of the training dataset dumper, the preferred way of doing so is to create a [fork](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html) of the repository.
You can use the fork to keep track of your changes and if you find them well-placed to be added to the `r22` branch, you can do so via a merge request.
In order to integrate changes to your target branch that may have been merged during the development of your changes, you may have to [rebase](https://docs.gitlab.com/ee/topics/git/git_rebase.html#git-rebase) your development branch from upstream. More information about rebasing can be found [here](https://www.atlassian.com/git/tutorials/rewriting-history/git-rebase).

### Package Layout

The code lives under [`BTagTrainingPreprocessing`](https://gitlab.cern.ch/atlas-flavor-tagging-tools/training-dataset-dumper/-/tree/r22/BTagTrainingPreprocessing). All the top-level
executables live in [`util/`](https://gitlab.cern.ch/atlas-flavor-tagging-tools/training-dataset-dumper/-/tree/r22/BTagTrainingPreprocessing/util), whereas various private internal classes
are defined in [`src/`](https://gitlab.cern.ch/atlas-flavor-tagging-tools/training-dataset-dumper/-/tree/r22/BTagTrainingPreprocessing/src).

### Configuration files

Jobs are steered via json files that live under [`configs`][cfgs]. Nearly all fields are mandatory and there are no default values.
In cases where the field contains a list, the field can (sometimes) be omitted to specify that the list is empty.

#### Fragments

Configuration files contain with one special key: `file`.
If this appears in a json object, the parser will assume the key
gives a path _relative to the current file_. Any entries in the file
will be imported at the same scope where the `file` key appears.
If local keys conflict with imported ones they will be merged, giving
local keys precedence over those imported from the file.

??? info "More on merging"
    Conflicting keys are merged as follows:

       - If the keys point objects (i.e. a `dict` or `map`), the union
         of all keys and values within the objects is taken, and
         conflicting keys are merged recursively.
       - If the keys point lists, the lists are concatenated.
       - If the keys point to anything else (i.e. numbers or strings),
         the local key overwrites the key from the file.

Fragments are a useful way to specify default values, since any values
imported via `file` will be overwritten by local ones.

[cfgs]: https://gitlab.cern.ch/atlas-flavor-tagging-tools/training-dataset-dumper/-/tree/r22/configs

#### Other configuration notes

 - The `n_jets_per_event` option will change the output format.
   If this is set to 0 we store one jet per entry. If it's set to
   a value larger than 0 each entry will be `n` jets, and a separate
   object with an `_event` suffix will store event information.

 - Both the top level configuration and the track dumpers require a
   `btagging_link` to be specified. This is the name of the link from
   jets to the `BTagging` object, and has historically been
   `btaggingLink` in most ATLAS code. Exceptionally, in the case of
   tracks, the link can be empty: in this case the tracks will be read
   directly from the jet.

### Adding More Outputs

There are two general steps for data flow in this package:

- **Augmenters** manipulate xAOD objects: they read in objects, calculate any properties of interest, and store (decorate) these properties on the same objects.
- **Writers** are responsible for xAOD -> HDF5 transcription: they read xAOD objects and write the associated data to HDF5 files.

We enforce this separation so that augmenters can easily be ported upstream to derivations, reconstruction, or the trigger. It also helps to keep the xAOD -> HDF5 transcription generic.

#### Adding an augmentation

There's an example class in [`src/DecoratorExample.cxx`](https://gitlab.cern.ch/atlas-flavor-tagging-tools/training-dataset-dumper/-/blob/r22/BTagTrainingPreprocessing/src/DecoratorExample.cxx) which should
make the implementation a bit more clear. This tool runs in the standard dumping script, so you can get a better picture of how it's implemented [by searching for it][de].

[de]: https://gitlab.cern.ch/search?search=DecoratorExample&group_id=7674&project_id=34016&scope=&search_code=true&snippets=false&repository_ref=r22

It should be easy to write out any simple (primitive type) decoration you add to a jet, the `BTagging` object, the `EventInfo` object, or a track.

#### Writing out an augmentation

The writer classes, like most code in this package, are configured via a json file.

Each configuration file has an object called
`"variables"`, which specifies the per-jet outputs. These are also specified
by type: there is one list for `"floats"`, one for `"chars"`,
etc. We assume that the variables are stored on the `BTagging` object by default, but there are also `"jet_int_variables"` and `"jet_floats"` for
anything on the jet itself. An `"event"` field specifies information that should be read off the `EventInfo` object.

Track-wise variables are specified within a similar structure within the `"tracks"` field.

Nothing is saved to output files by default: you need to add whatever
you've decorated to the b-tagging object to the output list. If the
output isn't found on the xAOD, the code should immediately throw an
exception.


### Editing Athena packages

You can modify existing Athena packages by building them locally
alongside the code in this package. You'll need to check out a local
copy of the `athena` repository. You should do this in the root
directory of the package source, i.e. alongside `docs/`, `configs`,
`README.md`, etc.

You can use [`git-fatlas`][fatlas]:

```bash
git-fatlas-init -r master
git-fatlas-add path/to/package
```

or use `git atlas` which should be accessible via `lsetup git`. Note that you might have to manually delete the `Projects` directory if you rely on `git atlas`.

### Editing the documentation

The documentation is provided by [mkdocs][mkd], and deployed via CERN Gitlab. For any larger edits to the documentation we recommend running mkdocs locally. It can be installed with pip:

```bash
pip install mkdocs mkdocs-material
```

and then launched from the root directory of this project

```bash
mkdocs serve
```

This will launch a server and provide you with a local URL to view the pages.

[fatlas]: https://github.com/dguest/git-fatlas
[mkd]: https://www.mkdocs.org/
