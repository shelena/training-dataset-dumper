This section describes how to run the basic AnalysisBase dataset dumper. This is the recommend workflow in almost all cases.

If you need to do something that depends on the ATLAS geometry service, or the ATLAS magnetic field (i.e. track extrapolation, running full b-tagging, or jet reconstruction), see the Advanced section.

### Running locally

After having followed the installation instructions and set up the training dataset dumper, you can invoke the executable with

```bash
dump-single-btag -c <path to configuration file> <paths to xAOD(s)>
```

The configuration files are located in [`configs/single-btag`](https://gitlab.cern.ch/atlas-flavor-tagging-tools/training-dataset-dumper/-/tree/r22/configs/single-b-tag).

### Running on the grid

In addition to the setup of the training dataset dumper described in the installation instructions, you need to go to the directory where you checked out the package and run

```bash
source training-dataset-dumper/BTagTrainingPreprocessing/grid/setup.sh
```

Then you are set up to submit jobs on the grid.
You can run the grid submission using this command:

```bash
submit-single-btag -t <tagname>
```

Use the `-h` flag to get more information about the arguments of this script. You can supply the configuration file for the jobs using `-c`, or by directly editing the script [`BTagTrainingPreprocessing/grid/submit-single-btag`](https://gitlab.cern.ch/atlas-flavor-tagging-tools/training-dataset-dumper/-/blob/r22/BTagTrainingPreprocessing/grid/submit-single-btag) before you submit. You can also modify this script to select which input samples which should be dumped.

If you are dumping samples that will be useful you should tag the current state of the reposity by using the `-t <tagname>` flag. 

Note that:

- The date will be preprended to the tag name you provide. 
- The tag will be pushed to your fork. 
- The script will error if you attempt to tag the base reposity.
