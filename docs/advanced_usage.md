The basic setup, described in the previous section, is capable of calculating simple variables from jets and b-tagging, and of running the neural network based flavor taggers. If you are starting from a DAOD that is likely all you need, **stop reading this and go to the basic section!** If you want to do something more complicated, read on.

The "advanced setup" supports a few additional workflows, e.g.:

- Augmenting release 21 (D)AODs to make them release 22 compatible,
- Extrapolating tracks to primary vertex to (re)calculate impact parameters
- Creating transient jet collections from trigger decisions, for
  trigger studies
- Building secondary vertices (i.e. "retagging") and running the full
  flavor tagging chain from untagged jets.

These workflows require you to use the Gaudi scheduler and event loop, part of the larger framework that ATLAS uses for trigger, reconstruction, and derivations.

For these cases we provide an Athena algorithm, `SingleBTagAlg`,
(technically a duel-use `EL::AnaAlgorithm`) which wraps the same
functionality as the stand alone executable.

### Installation

Further complicating matters, you can choose to build against either:

- `AthAnalysis`: the lightweight "analysis" Gaudi-based framework, or
- `Athena`: the full reconstruction framework

The lighter `AthAnalysis` framework will not support lower level reconstruction. For an exact list of which packages are included in every base project, see the `package_filters.txt` files under [the `Projects` directory in Athana][prj]. On the other hand, it's possible to run `AthAnalysis` locally via a docker image.

[prj]: https://gitlab.cern.ch/atlas/athena/-/tree/master/Projects

The training dataset dumper can be used within either release by using `setup-[project].sh` in place of
`setup-analysisbase.sh`. Make sure you rebuild your code _completely_ (delete the `build` directory) if you've changed releases!

Using the `Athena` project as an example, a local installation can be achieved with:

```bash
git clone ssh://git@gitlab.cern.ch:7999/atlas-flavor-tagging-tools/training-dataset-dumper.git
source training-dataset-dumper/setup-athena.sh
mkdir build
cd build
cmake ../training-dataset-dumper
make
```

### Running

A very simple `ComponentAccumulator`-based script to run it is provided in [`ca-dump-single-btag`](https://gitlab.cern.ch/atlas-flavor-tagging-tools/training-dataset-dumper/-/blob/r22/BTagTrainingPreprocessing/bin/ca-dump-single-btag).
You can test it with

```bash
test-dumper eventloop
```

There are a number of other scripts matching `ca-*`: these are based around `ComponentAccumulator` Athena configuration.
Use the built in help (e.g. `ca-dump-single-btag -h`) to learn more about them.

### Running over heavy ion xAODs

You need to run under athanalysis (use setup-athanalysis.sh).

Since HI xAOD are still produced under R21 we can dump using:
ca-dump-upgrade-HI -c <path to configuration file> <paths to xAOD(s)>

Configuration file should be upgrade-HI.json, that will use single-btag-variables-HI.json. Flag do_heavions must be set to true in upgrade-HI.json.

To run in grid use:
```bash
submit-HI
```

### Producing trigger training datasets

You can make trigger training datasets in Athena with

```bash
ca-dump-trigger-btag -c <dumper>/configs/single-b-tag/trigger.json xAOD.root
```

This is a _much_ heavier job than the standard ones, because it has to
load in a number of dependencies for track extrapolation. As with the
other scripts, see the `-h` for more options.

There is also a script `submit-trigger` to submit these jobs to the
grid.

### Working with nightlies

If you're on the bleeding edge of ATLAS software, there may not be a stable release that supports a new feature you need.
The scripts `setup-athena-latest.sh` and `setup-analysisbase-latest.sh` will set up the latest nightly build.

Working with nightlies comes with a few caveats:

- We make no promises that the code will work with them! Our continuous integration tests all updates in the tagged releases for both AnalysisBase and AthAnalysis based projects. This is not extended to nightlies.
- Nightlies will disappear after a few weeks. Because of this, **producing larger datasets** based on a nightly is **strongly discoursged:** no one will be able to reproduce your work when the nightly is deleted.
