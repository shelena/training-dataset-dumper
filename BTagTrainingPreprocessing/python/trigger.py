from AthenaConfiguration.ComponentFactory import CompFactory
from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator

# only works in full athena for now
from TrigDecisionTool.TrigDecisionToolConfig import getTrigDecisionTool

from math import inf

def getLabelingBuilderAlg(cfgFlags):
    label_tools = []
    for ptype in ['BHadrons','CHadrons','Taus']:
        tool = CompFactory.CopyFlavorLabelTruthParticles(f'{ptype}Builder')
        tool.ParticleType = f'{ptype}Final'
        tool.OutputName = f'TruthLabel{ptype}Final'
        tool.OutputLevel = cfgFlags.Exec.OutputLevel
        label_tools.append(tool)
    labelCollectionBuilder = CompFactory.JetAlgorithm("LabelBuilderAlg")
    labelCollectionBuilder.Tools = label_tools
    labelCollectionBuilder.OutputLevel = cfgFlags.Exec.OutputLevel
    return labelCollectionBuilder


def getDRTool(cfgFlags):
    dRTool = CompFactory.ParticleJetDeltaRLabelTool(
        "jetdrlabeler",
        LabelName = "HadronConeExclTruthLabelID",
        DoubleLabelName = "HadronConeExclExtendedTruthLabelID",
        BLabelName = "ConeExclBHadronsFinal",
        CLabelName = "ConeExclCHadronsFinal",
        TauLabelName = "ConeExclTausFinal",
        BParticleCollection = "TruthLabelBHadronsFinal",
        CParticleCollection = "TruthLabelCHadronsFinal",
        TauParticleCollection = "TruthLabelTausFinal",
        PartPtMin = 5000.0,
        JetPtMin =     0.0,
        DRMax = 0.3,
        MatchMode = "MinDR",
        OutputLevel = cfgFlags.Exec.OutputLevel
    )
    return dRTool

def getFixedConeTrackAssociationAlgs(
        cfgFlags, jc, bc, tpc='HLT_IDTrack_FS_FTF', an='FsTracks'):
    acc = ComponentAccumulator()
    Associator = CompFactory.JetParticleShrinkingConeAssociation
    trackOnJetDecorator = f'{jc}.{an}ForBTagging'
    # the formula here is R = p1 + exp(p2 + p3 * pt)
    assoc_tool = Associator(
        'FSTrackConeTool',
        coneSizeFitPar1=0.5,
        coneSizeFitPar2=-inf,   # exp(-inf) -> 0
        coneSizeFitPar3=0,      # fixed cone
        JetContainer=jc,
        InputParticleContainer=tpc,
        OutputDecoration=trackOnJetDecorator.split('.')[-1],
    )
    assoc_alg = CompFactory.JetDecorationAlg(
        'FSTrackAssociationAlg',
        JetContainer=jc,
        Decorators=[assoc_tool])
    acc.addEventAlgo(assoc_alg)

    Copier = CompFactory.FlavorTagDiscriminants.BTagTrackLinkCopyAlg
    copier = Copier(
        'FSTrackCopier',
        jetTracks=trackOnJetDecorator,
        btagTracks=f'{bc}.{an}',
        jetLinkName=f'{bc}.jetLink'
    )
    acc.addEventAlgo(copier)
    return acc

# We do imports in this function because it's expensive to set up and
# only works in Athena. It should only be required for the btagIp_*
# variables.
def getTrackAugmentation(
        cfgFlags,
        tpc='HLT_IDTrack_FS_FTF',
        pvc='HLT_IDVertex_FS'):
    try:
        from BTagging.BTagTrackAugmenterAlgConfig import (
            BTagTrackAugmenterAlgCfg as TrackAugCfg)
        return TrackAugCfg(
            cfgFlags,
            TrackCollection=tpc,
            PrimaryVertexCollectionName=pvc)
    except ModuleNotFoundError as err:
        print(f'WARNING: problem setting up track augmentation: {err}')
        return ComponentAccumulator()


def triggerJetGetterCfg(cfgFlags, chain, temp_jets, temp_btag):
    ca = ComponentAccumulator()

    # We have two ways to set up the trigger decision tool, because it
    # works differently in full Athena and in AthAnalysis.
    try:
        # First the Athena way. This sets up the trigger decision tool
        # the way Tim Martin recommends.
        trigDecTool = getTrigDecisionTool(cfgFlags).getPrimary()
    except RuntimeError:
        # This is the fallback way since the above function fails in
        # AthAnalysis. As of 2021.03.25 this version only works for
        # the first file, then crashes in one or another terrible way.
        trigDecTool = CompFactory.Trig.TrigDecisionTool('TrigDecisionTool')
        trigDecTool.NavigationFormat = "TrigComposite" # Run 3 style

    # Local component to move trigger elements into collections so we
    # can access them inside offline code
    jetGetter = CompFactory.TriggerJetGetterAlg(
        'TriggerJetGetterAlg')
    jetGetter.triggerDecisionTool = trigDecTool
    jetGetter.bJetChain = chain
    jetGetter.outputJets = temp_jets
    jetGetter.outputBTag = temp_btag
    jetGetter.jetModifiers = [getDRTool(cfgFlags)]
    jetGetter.TrigJetCollectionKey = "HLT_.*"
    ca.addEventAlgo(jetGetter)

    return ca
