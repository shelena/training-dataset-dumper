#!/usr/bin/env python3

"""
Dump the menu!

Dumps to stdout by default. Also turns off all the logging stuff, so
you can pipe things if you feel so inclined.
"""


from AthenaConfiguration.AutoConfigFlags import GetFileMD
from argparse import ArgumentParser
import sys
import json
import logging


def get_args():
    parser = ArgumentParser(description=__doc__)
    parser.add_argument('file')
    output = parser.add_mutually_exclusive_group()
    types=dict(action='store_const', dest='output')
    output.add_argument('-t', '--text', const='text', **types)
    output.add_argument('-j', '--json', const='json', **types)
    parser.set_defaults(output='text')
    return parser.parse_args()

def dump_text(menu, stream=sys.stdout):
    for key, things in menu.items():
        stream.write(f'{key}:\n')
        for thing in things:
            stream.write(f' {thing}\n')

def run():
    args = get_args()

    # turn off logs
    for logger in ['AutoConfigFlags', 'MetaReader']:
        log = logging.getLogger(logger)
        log.setLevel(logging.WARNING)

    m = GetFileMD([args.file])
    menu = m['TriggerMenu']
    stream = sys.stdout
    if args.output == 'text':
        dump_text(menu, stream)
    elif args.output == 'json':
        json.dump(menu, stream, indent=2)

if __name__ == '__main__':
    run()

