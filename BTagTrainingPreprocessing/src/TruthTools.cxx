#include "TruthTools.hh"

#include "AthContainers/ConstDataVector.h"

namespace truth {

  bool is_from_WZ(const xAOD::TruthParticle &truth_particle) {
    for (std::size_t parent_index = 0; parent_index < truth_particle.nParents(); parent_index++) {
      const xAOD::TruthParticle* pptr = truth_particle.parent(parent_index);
      if (!pptr) {
        std::string barcode = std::to_string(truth_particle.barcode());
        std::string idx = std::to_string(parent_index);
        throw TruthRecordError(
          "parent " + idx + " of particle barcode " + barcode
          + " is a null pointer");
      }
      const xAOD::TruthParticle& parent = *pptr;
      if (parent.isW() || parent.isZ() || is_from_WZ(parent)) return true;
    }
    return false;
  }
  std::vector<const xAOD::TruthParticle*> getLeptonsFromWZ(
    const std::vector<const xAOD::TruthParticle*>& in) {

    std::vector<const xAOD::TruthParticle*> out;
    for (const xAOD::TruthParticle* truth_particle: in) {

      // protection for slimmed event record
      if ( ! truth_particle) throw TruthRecordError(
        "truth particle is a null pointer");

      // check for a stable lepton
      if (! (truth_particle->isElectron() ||
             truth_particle->isMuon()) ) continue;
      if (truth_particle->status() != 1) continue;

      // check that it came from a W or Z decay
      if (is_from_WZ(*truth_particle)) out.push_back(truth_particle);
    }
    return out;
  }

  bool is_overlaping_lepton(
    const xAOD::Jet &jet,
    const std::vector<const xAOD::TruthParticle*>& tpv,
    float dR) {

    for (const xAOD::TruthParticle* truth_particle: tpv) {

      if (truth_particle->pt() < 10000) continue;
      if (jet.p4().DeltaR(truth_particle->p4()) > dR) continue;

      // ...from a W
      if (is_from_WZ(*truth_particle)) return true;
    } // end of particle loop
    return false;
  }
  
  bool isWeaklyDecayingHadron(const xAOD::TruthParticle& part)
  {
    if ( isWeaklyDecayingHadron(part, 5) ) { return true; }
    if ( isWeaklyDecayingHadron(part, 4) ) { return true; }
    return false;    
  }

  bool isWeaklyDecayingHadron(const xAOD::TruthParticle& part, int flav)
  {
    if ( isHadron(part, flav) ) {

      if ( not part.hasDecayVtx() ) { return false; }
      const xAOD::TruthVertex* vx = part.decayVtx();
      for ( size_t i = 0; i < vx->nOutgoingParticles(); i++ ) {
        const xAOD::TruthParticle* outPart = vx->outgoingParticle(i);
        if ( isHadron(*outPart, flav) ) { return  false; }
      } 
      return true;
    }
    return false;
  }

  bool isHadron(const xAOD::TruthParticle& part, int flav)
  {
    if( flav == 5 and part.isBottomHadron() ) { return true; }
    if( flav == 4 and part.isCharmHadron() )  { return true; }
    return false;
  }

  bool isFinalStateLepton(const xAOD::TruthParticle& part) 
  {
    if ( part.status() != 1  ) { return false; }
    if ( not part.isLepton() ) { return false; }
    return true;
  }

}
