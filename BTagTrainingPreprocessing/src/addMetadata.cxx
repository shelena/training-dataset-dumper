#include "addMetadata.hh"

#include "H5Cpp.h"

#include "CutBookkeeperUtils/OriginalAodCounts.h"
#include "HDF5Utils/Writer.h"


void addMetadata(H5::Group& grp, const OriginalAodCounts& counts){
  H5Utils::Consumers<const OriginalAodCounts&> cons;
#define ADD(NAME, TYPE)\
  cons.add<TYPE>(#NAME,[](const OriginalAodCounts& c) {return c.NAME;})
  ADD(nEventsProcessed, long long);
  ADD(sumOfWeights, double);
  ADD(sumOfWeightsSquared, double);
  ADD(nIncomplete, int);
#undef ADD
  H5Utils::Writer<0, const OriginalAodCounts&> writer(grp, "metadata", cons);
  writer.fill(counts);
}

