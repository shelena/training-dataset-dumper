#include "ConfigFileTools.hh"
#include "BTagTrackWriterConfig.hh"

#include <cmath> // NAN
#include <set>

// Jan 7th, 2021: This define is needed before the json parser to
// silence a warning, it might go away with more recent versions of
// boost. Please consider removing the define below if you're reading
// this substantially after this date.
//
#define BOOST_BIND_GLOBAL_PLACEHOLDERS
#include <boost/property_tree/json_parser.hpp>

namespace ConfigFileTools {
  bool boolinate(const boost::property_tree::ptree& pt,
                 const std::string& key) {
    std::string val = pt.get<std::string>(key);
    if (val == "true") return true;
    if (val == "false") return false;
    throw std::logic_error("'" + key + "' should be 'true' or 'false'");
  }
  bool boolinate_default(const boost::property_tree::ptree& pt,
                         const std::string& key,
                         bool default_value) {
    if (! pt.count(key) ) return default_value;
    return boolinate(pt, key);
  }
  std::vector<std::string> get_list(const ptree& pt) {
      std::vector<std::string> vars;
      for (const auto& var: pt) {
        vars.push_back(var.second.get_value<std::string>());
      }
      return vars;
  }
  std::set<std::string> get_key_set(const ptree& pt) {
    std::set<std::string> keys;
    for (const auto& pair: pt) {
      keys.insert(pair.first);
    }
    return keys;
  }
  void throw_if_any_keys_left(const std::set<std::string>& keys,
                              const std::string& type = "keys") {
    if (keys.empty()) return;
    std::string problem = "Found unknown " + type + " in configuration: ";
    for (const auto& k: keys) {
      problem.append(k);
      if (k != *keys.rbegin()) problem.append(", ");
    }
    throw std::runtime_error(problem);
  }
  MapOfLists get_variable_list(const boost::property_tree::ptree& pt) {
    MapOfLists var_map;
    for (const auto& node: pt) {
      var_map[node.first] = get_list(node.second);
    }
    return var_map;
  }
  TrackWriterVariables get_track_variables(const ptree& pt) {
    TrackWriterVariables vars;
    std::set<std::string> keys = get_key_set(pt);
    auto get = [&keys, &pt](const std::string& name) {
      if (keys.erase(name)) return get_list(pt.get_child(name));
      return std::vector<std::string>();
    };
#define FILL(field) vars.field = get(#field)
    FILL(uchars);
    FILL(ints);
    FILL(halves);
    FILL(floats);
    FILL(customs);
    FILL(half_precision_customs);
#undef FILL
    throw_if_any_keys_left(keys, "track variable types");
    return vars;
  }


  // Merging lists is just appending them for now
  boost::property_tree::ptree merge_lists(
    const boost::property_tree::ptree& oldtree,
    const boost::property_tree::ptree& newtree) {
    namespace pt = boost::property_tree;
    pt::ptree out;
    for (const auto& tree: {oldtree, newtree}) {
      for (const auto& item: tree) {
        out.add_child("",item.second);
      }
    }
    return out;
  }

  void merge_trees(boost::property_tree::ptree& oldtree,
                   const boost::property_tree::ptree& newtree) {
    if (oldtree.data().empty() != newtree.data().empty()) {
      throw std::logic_error("tried to merge two incompatable trees");
    }

    // trees with data are primative types, assign and return
    if (!oldtree.data().empty()) {
      if (oldtree.size() > 0 || newtree.size() > 0) {
        throw std::logic_error(
          "primative config values should not have children");
      }
      oldtree.data() = newtree.data();
      return;
    }
    // check if this is a list, i.e. all keys are blank
    if (newtree.count("") == newtree.size()) {
      oldtree = merge_lists(oldtree, newtree);
      return;
    }
    // if we got here it's an object
    for (const auto& subnew: newtree) {
      // add new entries
      if (!oldtree.count(subnew.first)) {
        oldtree.put_child(subnew.first, subnew.second);
      } else {
        // if both trees have this object, try to merge them
        merge_trees(oldtree.get_child(subnew.first), subnew.second);
      }
    }
  }

  // if any of the variable lists have a node called "file" we read in
  // that file and replace the node with the contents.
  void combine_files(boost::property_tree::ptree& node,
                     boost::filesystem::path config_path) {
    namespace fs = boost::filesystem;
    namespace pt = boost::property_tree;
    for (auto& subnode: node) {
      combine_files(subnode.second, config_path);
    }
    if (!node.count("file")) return;
    pt::ptree local = node;
    fs::path file_path(node.get<std::string>("file"));

    // if the file doesn't exist in this directory, check the one
    // where the root configuration file was stored
    if (!fs::exists(file_path)) {
      file_path = config_path.parent_path() / file_path;
    }

    pt::ptree file;
    pt::read_json(file_path.string(), file);
    combine_files(file, file_path);
    node = file;

    // now add in anything else that was specified after the file
    merge_trees(node, local);
    node.erase("file");
  }

  float null_as_nan(const boost::property_tree::ptree& node,
                    const std::string& key) {
    const auto& child = node.get_child(key);
    if (child.data() == "null") return NAN;
    return child.get_value<float>();
  }

  DL2Config get_dl2_config(const boost::property_tree::ptree& node) {
    std::map<std::string,std::string> remapping;
    for (const auto& remappt: node.get_child("remapping")) {
      remapping[remappt.first] = remappt.second.data();
    }
    return {node.get<std::string>("nn_file_path"), remapping};
  }

  std::map<std::string, std::string> check_map_from(
    const MapOfLists& default_to_vals){
    std::map<std::string, std::string> out;
    for (const auto& pair: default_to_vals) {
      const std::string& check_var = pair.first;
      for (const auto& var: pair.second) {
        if (out.count(var)) {
          throw std::logic_error(var + " already in map");
        }
        out[var] = check_var;
      }
    }
    return out;
  }

}
