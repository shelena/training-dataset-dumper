#ifndef HIT_WRITER_HH
#define HIT_WRITER_HH

// Standard Library things
#include <string>
#include <vector>
#include <memory>
 
#include "xAODJet/Jet.h"
#include "xAODTracking/TrackMeasurementValidationContainer.h"

namespace H5 {
  class Group;
}

namespace xAOD {
  class Jet_v1;
  typedef Jet_v1 Jet;
  class TrackMeasurementValidation_v1;
  typedef TrackMeasurementValidation_v1 TrackMeasurementValidation;
  class Vertex_v1;
  typedef Vertex_v1 Vertex;
}

class HitWriterConfig;
class HitOutputWriter;
class HitConsumers;

struct HitOutputs {
  const xAOD::TrackMeasurementValidation* hit;
  const xAOD::Jet* jet;
  const xAOD::Vertex* pv;
};

class HitWriter
{
public:
  HitWriter(
    H5::Group& output_file,
    const HitWriterConfig&);
  ~HitWriter();
  HitWriter(HitWriter&) = delete;
  HitWriter operator=(HitWriter&) = delete;
  HitWriter(HitWriter&&);
  void write(const xAOD::TrackMeasurementValidationContainer& hits, 
  	     const xAOD::Jet& jet,
	     const xAOD::Vertex& vx);
  void write_dummy();
private:
  void add_hit_variables(HitConsumers&);
  std::unique_ptr<HitOutputWriter> m_hdf5_hit_writer;
  float m_dR_to_jet;
};

#endif
