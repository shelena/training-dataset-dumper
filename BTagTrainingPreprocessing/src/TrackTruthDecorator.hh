#ifndef TRACK_TRUTH_DECORATOR_HH
#define TRACK_TRUTH_DECORATOR_HH

#include "TrackSelector.hh"

#include "InDetTrackSystematicsTools/InDetTrackTruthOriginTool.h"
#include "InDetTrackSystematicsTools/InDetTrackTruthOriginDefs.h"

#include "xAODTracking/TrackParticle.h"
#include "xAODTracking/TrackParticleContainer.h"

#include "xAODTruth/TruthParticle.h"
#include "xAODTruth/TruthParticleContainer.h"

#include "xAODTruth/TruthVertex.h"
#include "xAODTruth/TruthVertexContainer.h"

#include "xAODTracking/VertexContainer.h"
#include "xAODTruth/TruthEventContainer.h"

#include <string>
#include <math.h>


// this is a bare-bones example of a class that could be used to
// decorate a track with additional information

class TrackTruthDecorator
{
public:
  TrackTruthDecorator(const std::string& decorator_prefix = "");

  // this is the function that actually does the decoration
  void decorateAll(TrackSelector::Tracks tracks, 
                   const xAOD::TruthVertex* truth_PV) const;

  // helper functions
  static bool sort_tracks(const xAOD::TrackParticle* track_A , const xAOD::TrackParticle* track_B );
  const xAOD::TruthVertex* get_truth_vertex( const xAOD::TrackParticle* track ) const;
  const xAOD::TruthVertex* get_nearest_vertex(const xAOD::TruthVertex* search_vertex, std::vector<const xAOD::TruthVertex*> vertices) const;
  
private:
  // All this class does is apply a decoration, so all it needs to do
  // is contain one decorator. We could have also written a function
  // and statically initalized this, but static data in functions is
  // probably best avoided.

  float min_dr_to_merge = 0.1;

  SG::AuxElement::Decorator<int> m_track_origin_label;
  SG::AuxElement::Decorator<int> m_track_production_vertex_idx;

  InDet::InDetTrackTruthOriginTool m_InDetTrackTruthOriginTool;

};

#endif
