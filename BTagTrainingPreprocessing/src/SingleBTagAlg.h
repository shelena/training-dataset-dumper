#include "xAODBTagging/BTaggingContainer.h"
#include "xAODJet/JetContainer.h"

#include "AnaAlgorithm/AnaAlgorithm.h"
#include "AsgDataHandles/ReadHandleKey.h"
#include "AsgDataHandles/ReadDecorHandleKey.h"

#include <memory>

class SingleBTagTools;
class SingleBTagConfig;
class SingleBTagOutputs;
namespace H5 {
  class H5File;
}

class SingleBTagAlg: public EL::AnaAlgorithm
{
public:
  SingleBTagAlg(const std::string& name, ISvcLocator* pSvcLocator);
  ~SingleBTagAlg();

  // these are the functions inherited from Algorithm
  virtual StatusCode initialize () override;
  virtual StatusCode execute () override;
  virtual StatusCode finalize () override;
private:
  std::string m_output_file_name;
  std::string m_config_file_name;
  std::string m_metadata_file_name;

  std::unique_ptr<SingleBTagConfig> m_config;

  std::unique_ptr<H5::H5File> m_output_file;
  std::unique_ptr<SingleBTagTools> m_tools;
  std::unique_ptr<SingleBTagOutputs> m_outputs;

  // stuff for data dependencies
  SG::ReadHandleKey<xAOD::JetContainer> m_jetKey{this, "jetKey", "", ""};
  std::vector<std::unique_ptr<SG::ReadDecorHandleKey<xAOD::BTagging>>> m_bDec;

};
