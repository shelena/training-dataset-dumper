#ifndef HIT_DECORATOR_HH
#define HIT_DECORATOR_HH

#include "AthContainers/DataVector.h"
#include "AthContainers/AuxElement.h"

namespace xAOD {
  class Jet_v1;
  using Jet = Jet_v1;
  class Vertex_v1;
  using Vertex = Vertex_v1;
  class TrackMeasurementValidation_v1;
  using TrackMeasurementValidationContainer =
    DataVector<TrackMeasurementValidation_v1>;
}

class HitDecorator
{
public:
  HitDecorator();
  void decorate(const xAOD::Jet&,
                const xAOD::TrackMeasurementValidationContainer&,
                const xAOD::Vertex&) const;
private:
  // you can add more decorators here. For things that aren't integers
  // you can create them with a template argument `float`.
  SG::AuxElement::Decorator<int> m_nHits_L0;
  SG::AuxElement::Decorator<int> m_nHits_L1;
  SG::AuxElement::Decorator<int> m_nHits_L2;
  SG::AuxElement::Decorator<int> m_nHits_L3;
};

#endif
