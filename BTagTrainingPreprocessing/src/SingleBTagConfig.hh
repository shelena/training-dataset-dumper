#ifndef SINGLEBTAG_CONFIG_HH
#define SINGLEBTAG_CONFIG_HH

#include "DL2Config.hh"

#include <string>
#include <vector>
#include <map>
#include "TrackSelectorConfig.hh"
#include "TruthSelectorConfig.hh"
#include "TrackSortOrder.hh"
#include "BTagTrackWriterConfig.hh"
#include "HitWriterConfig.hh"

typedef std::map<std::string,std::vector<std::string>> VariableList;

enum class JetCleanOption {none, event, jet};

struct TrackConfig {
  size_t n_to_save;
  TrackSortOrder sort_order;
  TrackSelectorConfig selection;
  TrackWriterVariables variables;
  std::string input_name;
  std::string output_name;
  std::string ip_prefix;
};

struct TruthConfig {
  size_t n_to_save;
  TrackSortOrder sort_order;
  TruthSelectorConfig selection;
  std::string output_name;
};

struct DecorateConfig {
  bool jet_aug;
  bool btag_jes;
  bool soft_muon;
  bool track_truth_info;
  bool track_sv_info;
  bool track_ambiguity_solver_ranking;
  bool hits;
};

struct SingleBTagConfig {
  std::string jet_collection;
  std::string jet_calibration_collection;
  std::string jet_calib_file;
  std::string cal_seq;
  std::string cal_area;
  bool do_heavyions;
  bool do_calibration;
  float jvt_cut;
  float pt_cut;
  std::string vertex_collection;
  std::string btagging_link;
  std::vector<DL2Config> dl2_configs;
  VariableList btag;
  std::map<std::string, std::string> default_flag_mapping;
  std::vector<TrackConfig> tracks;
  std::vector<TruthConfig> truths;
  HitWriterConfig hits;
  size_t n_jets_per_event;
  DecorateConfig decorate;
  JetCleanOption jetclean_option;
};

SingleBTagConfig get_singlebtag_config(const std::string& config_file_name);


#endif
