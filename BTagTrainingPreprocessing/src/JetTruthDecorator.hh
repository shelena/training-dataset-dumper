#ifndef JETTRUTHDECORATOR_HH
#define JETTRUTHDECORATOR_HH

#include "TruthSelectorConfig.hh"

#include "xAODJet/JetFwd.h"
#include "xAODTruth/TruthParticleFwd.h"
#include "xAODTruth/TruthParticleContainer.h"

#include "AthContainers/AuxElement.h"
#include "AthLinks/ElementLink.h"

#include <string>


class JetTruthDecorator
{
public:
  typedef SG::AuxElement AE;
  typedef ElementLink<xAOD::TruthParticleContainer> TruthLink;
  typedef std::vector<TruthLink> TruthLinks;

  JetTruthDecorator(const std::string& link_name, TruthSelectorConfig = TruthSelectorConfig());

  // this is the function that actually does the decoration
  void decorate(const xAOD::Jet& jet, const xAOD::TruthParticleContainer* tpc) const;

  bool passed_cuts(const xAOD::TruthParticle& part, const xAOD::Jet& jet) const;

private:
  // All this class does is apply a decoration, so all it needs to do
  // is contain one decorator. We could have also written a function
  // and statically initalized this, but static data in functions is
  // probably best avoided.
  SG::AuxElement::Decorator<TruthLinks> m_deco;
  TruthSelectorConfig m_truth_select_cfg;
  std::string m_out_name;

};

#endif
