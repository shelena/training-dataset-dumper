#include "processSingleBTagEvent.hh"

// the implementation is the same for every signature, we just have to
// recompile it a few times below. We do it here to avoid having the
// template headers interfere with anything that calls this function.
#include "SingleBTagTools.hh"
#include "SingleBTagConfig.hh"
#include "BJetShallowCopier.hh"
#include "JetTruthDecorator.hh"
#include "TruthTools.hh"

#include "xAODRootAccess/TEvent.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODJet/JetContainer.h"
#include "xAODTracking/TrackParticleContainer.h"
#include "xAODTracking/TrackMeasurementValidationContainer.h"

#include "xAODHIEvent/HIEventShapeAuxContainer.h"
#include "xAODHIEvent/HIEventShapeContainer.h"

#include "PathResolver/PathResolver.h"

namespace {

  void check_rc(StatusCode code) {
    if (!code.isSuccess()) throw std::runtime_error("bad return code");
  }

  constexpr float operator "" _GeV(unsigned long long d) { return d*1e3; }
  constexpr float operator "" _GeV(long double d) { return d*1e3; }

  const xAOD::Vertex* primary(const xAOD::VertexContainer& vertices) {
    if (vertices.size() == 0) {
      throw std::runtime_error("no primary vertices");
    }
    for ( const xAOD::Vertex *vertex : vertices ) {
      if ( vertex->vertexType() == xAOD::VxType::PriVtx ) {
        return vertex;
      }
    }
    // if we find nothing else this should be the beam spot
    return vertices.front();
  }
}

// this is the templated code, the concrete instances are below
template <typename Event>
void processSingleBTagEventImpl(Event& event,
                                const SingleBTagConfig& jobcfg,
                                /*const*/ SingleBTagTools& tools,
                                SingleBTagOutputs& out) {
  const xAOD::EventInfo *event_info = nullptr;
  check_rc( event.retrieve(event_info, "EventInfo") );

   if (jobcfg.do_heavyions) {
    //retrieve FCal ET for collisions centrality percentiles. do_heavyions(true) is needed.
    const xAOD::HIEventShapeContainer* calos = 0;
    check_rc( event.retrieve(calos, "CaloSums") );
    tools.dec.fcal_et_tev(*event_info) = calos->at(5)->et() * 1e-6; // MeV->TeV
  } 

  // jet event cleaning: do not process events with bad jets
  if (jobcfg.jetclean_option == JetCleanOption::event) {
    bool result = tools.acc.eventClean_looseBad(*event_info);
    // do not process event, if event-level jet cleaning flag is not passed
    if (!result) return;
  }

  // we'll need truth particles and event info
  const xAOD::TruthParticleContainer* tpc = nullptr;
  std::vector<const xAOD::TruthParticle*> truth_leptons;
  std::vector<const xAOD::TruthParticle*> truth_particles;
  try {
    check_rc( event.retrieve(tpc, "TruthParticles") );
    truth_particles = std::vector<const xAOD::TruthParticle*>(
      tpc->begin(), tpc->end());
    truth_leptons = truth::getLeptonsFromWZ(truth_particles);
    out.truth_counts.n_successful_truth_reads++;
  } catch (truth::TruthRecordError& err) {
    std::cerr << getEventInfoString(err, *event_info) << std::endl;
    out.truth_counts.n_failed_truth_reads++;
    return;
  }

  // read the jets
  const xAOD::JetContainer *raw_jets = nullptr;
  check_rc( event.retrieve(raw_jets, jobcfg.jet_collection) );

  const xAOD::TrackMeasurementValidationContainer* hits = nullptr;
  // Check whether we want to save the hits with HitWriter or we want
  // to decorate the jets with the hit multiplicities
  if (out.hits || jobcfg.decorate.hits) check_rc(
    event.retrieve(hits, "JetAssociatedPixelClusters"));
  // first loop: add decorations to jets. These have to be done
  // before calibration to be consistent with reconstruction
  auto [jets, aux] = tools.shallow_copier.shallowCopyBJets(*raw_jets);
  for (const xAOD::Jet* uncalib_jet: *jets) {

    // this is more important stuff
    const xAOD::BTagging* btag = *tools.acc.btaggingLink(*uncalib_jet);
    if (!btag) throw std::runtime_error("no btaggingLink");

    if (jobcfg.decorate.jet_aug) {
      tools.jet_augmenter.augment(*btag, *btag);
    }
    if (jobcfg.decorate.soft_muon) {
      tools.muon_augmenter.augment(*btag);
    }
    if (jobcfg.decorate.btag_jes){
      tools.jet_augmenter.augmentBtagJes(*btag, *btag);
    }
    for (const auto& dl2: tools.dl2s) {
      dl2.decorate(*btag);
    }

    // decorate truth particles
    if (tools.hadrons) tools.hadrons->decorate(*uncalib_jet, tpc);
    if (tools.leptons) tools.leptons->decorate(*uncalib_jet, tpc);
  }

  if (jobcfg.do_calibration) {
    check_rc(tools.calibration_tool.applyCalibration(*jets));
  }

  // sort jets by descending pt
  // we make a new container first to preserve the indices
  std::vector<const xAOD::Jet*> sorted_jets(jets->begin(), jets->end());
  std::sort(sorted_jets.begin(), sorted_jets.end(),
            [](const auto* j1, const auto* j2) {
              return j1->pt() > j2->pt();
            });

  // we need the pv to add the correct hits to jets
  const xAOD::VertexContainer* primary_vertices = nullptr;
  check_rc( event.retrieve(primary_vertices, jobcfg.vertex_collection));
  const xAOD::Vertex* pv = primary(*primary_vertices);
  if (jobcfg.decorate.hits) {
    for (const xAOD::Jet* jet: sorted_jets) {
      tools.hit_decorator.decorate(*jet, *hits, *pv);
    }
  }

  // decorate all tracks
  if ( jobcfg.decorate.track_ambiguity_solver_ranking ) {
    const xAOD::TrackParticleContainer* tracks = nullptr;
    check_rc( event.retrieve(tracks, "InDetTrackParticles"));
    tools.trkAmbiDecorator.decorateAll(*tracks);
  }

  // save some primary vertex information on eventinfo
  tools.dec.n_primary_vertices(*event_info) = primary_vertices->size();
  tools.dec.primary_vertex_detector_z(*event_info) = pv->z();
  tools.dec.primary_vertex_detector_z_uncertainty(*event_info) = (
    std::sqrt(pv->covariancePosition()(2,2)));
  const xAOD::TruthEventContainer* truthEventContainer = nullptr;
  const xAOD::TruthVertex* truth_PV = nullptr;
  if ( jobcfg.decorate.track_truth_info ) {
    check_rc( event.retrieve(truthEventContainer, "TruthEvents"));
    // truthEventContainer always has size == 1?
    truth_PV = truthEventContainer->at(0)->truthVertex(0);
  }

  // second loop: select calibrated jets and write out to HDF5
  std::vector<const xAOD::Jet*> jets_to_write;
  unsigned int rank = 0;
  for (const xAOD::Jet* calib_jet: sorted_jets) {
    if (truth::is_overlaping_lepton(*calib_jet, truth_leptons, 0.3)) {
      continue;
    }

    // get the b-tagging object
    const xAOD::BTagging* btag = *tools.acc.btaggingLink(*calib_jet);

    tools.dec.jet_rank(*calib_jet) = rank++;
    if (jobcfg.do_calibration){
      // if we're calibrating jets we need to check the JVT again
      float updated_jvt_value= tools.jvttool.updateJvt(*calib_jet);
      tools.dec.jvt(*calib_jet) = updated_jvt_value;
      bool fail_jvt = (
        calib_jet->pt() > 20_GeV &&
        calib_jet->pt() < 60_GeV &&
        std::abs(calib_jet->eta()) < 2.4 &&
        updated_jvt_value < jobcfg.jvt_cut );
      if (fail_jvt) continue;

      // only do jet-level jet cleaning if not doing event-level jet cleaning
      if (jobcfg.jetclean_option == JetCleanOption::jet &&
          !tools.cleaning_tool.keep(*calib_jet)) continue;
    }
    else {
      tools.dec.jvt(*calib_jet) = NAN;
    }

    if (calib_jet->pt() < jobcfg.pt_cut || std::abs(calib_jet->eta()) > 2.5) {
      continue;
    }

    // write out tracks associated with jets
    for (auto& tracktool: out.tracks ) {
      const xAOD::Jet* uncalib_jet = raw_jets->at(calib_jet->index());
      auto tracks = tracktool.selector.get_tracks(*uncalib_jet);
      for (const auto& track: tracks) {
        if ( jobcfg.decorate.track_sv_info ) {
              tools.trkVertexDecorator.decorate(*track, *btag, *pv);
        }
      }
      if ( jobcfg.decorate.track_truth_info ) {
            tools.trkTruthDecorator.decorateAll(tracks, truth_PV);
      }

      // TODO: remove the sort function's reliance on this decoration
      for (const auto& track: tracks) {
        tracktool.track_accessor.augment_with_ip(*track, *uncalib_jet);
      }
      sort(tracks.begin(), tracks.end(), tracktool.sort);

      tracktool.writer.write(tracks, *uncalib_jet);
      tracktool.n_tracks_decorator(*calib_jet) = tracks.size();
    }

    // write hits
    if (out.hits) {
      const xAOD::Jet* uncalib_jet = raw_jets->at(calib_jet->index());
      out.hits->write(*hits, *uncalib_jet, *pv);
    }

    // write truth particles
    for (auto& truthtool: out.truths) {
      const xAOD::Jet* uncalib_jet = jets->at(calib_jet->index());
      truthtool.write(*uncalib_jet);
    }

    // collect jets for output
    jets_to_write.push_back(calib_jet);
  }

  // write out jets
  if (!jets_to_write.empty()) {
    out.jet_writer.write(jets_to_write, event_info);
  }

}

// Concrete versions of the templated function above. These are the
// ones that are exposed to be used in other files.
void processSingleBTagEvent(xAOD::TEvent& e,
                            const SingleBTagConfig& c,
                            /*const*/ SingleBTagTools& t,
                            SingleBTagOutputs& o) {
  processSingleBTagEventImpl(e, c, t, o);
}

// StoreGateSvc isn't defined in AnalysisBase...
#ifndef XAOD_STANDALONE
void processSingleBTagEvent(StoreGateSvc& e,
                            const SingleBTagConfig& c,
                            /*const*/ SingleBTagTools& t,
                            SingleBTagOutputs& o) {
  processSingleBTagEventImpl(e, c, t, o);
}
#else
// ... but SgTEvent is
void processSingleBTagEvent(asg::SgTEvent& e,
                            const SingleBTagConfig& c,
                            /*const*/ SingleBTagTools& t,
                            SingleBTagOutputs& o) {
  processSingleBTagEventImpl(e, c, t, o);
}
#endif  // XAOD_STANDALONE
