#ifndef SINGLE_BTAG_TOOLS_HH
#define SINGLE_BTAG_TOOLS_HH

#include <optional>


#include "BTagJetWriter.hh"
#include "BTagTrackWriter.hh"
#include "TrackSelector.hh"
#include "trackSort.hh"
#include "TruthWriter.hh"
#include "DecoratorExample.hh"
#include "JetTruthDecorator.hh"
#include "TrackTruthDecorator.hh"
#include "TrackVertexDecorator.hh"
#include "TrackAmbiDecorator.hh"
#include "TruthCorruptionCounter.hh"
#include "HitDecorator.hh"
#include "BJetShallowCopier.hh"
#include "HitWriter.hh"
#include "JetTruthDecorator.hh"
#include "TruthSelectorConfig.hh"

#include "FlavorTagDiscriminants/BTagJetAugmenter.h"
#include "FlavorTagDiscriminants/BTagTrackIpAccessor.h"
#include "FlavorTagDiscriminants/BTagMuonAugmenter.h"
#include "FlavorTagDiscriminants/DL2HighLevel.h"

#include "JetCalibTools/JetCalibrationTool.h"
#include "JetSelectorTools/JetCleaningTool.h"
#include "JetMomentTools/JetVertexTaggerTool.h"
#include "InDetTrackSelectionTool/InDetTrackSelectionTool.h"

struct SingleBTagConfig;
struct TrackConfig;
struct TruthConfig;

namespace H5 {
  class H5File;
}

struct TrackToolkit {
  using AE = SG::AuxElement;
  TrackToolkit(const TrackConfig&, H5::H5File&);
  TrackToolkit(TrackToolkit&&);
  TrackSelector selector;
  TrackSort sort;
  BTagTrackWriter writer;
  BTagTrackIpAccessor track_accessor;
  SG::AuxElement::Decorator<int> n_tracks_decorator;
};

struct SingleBTagTools {

  using AE = SG::AuxElement;

  struct Accessors {
    Accessors(const SingleBTagConfig& cfg);
    AE::ConstAccessor<char> eventClean_looseBad;
    AE::ConstAccessor<ElementLink<xAOD::BTaggingContainer>> btaggingLink;
  };

  struct Decorators {
    AE::Decorator<float> jvt;
    AE::Decorator<float> fcal_et_tev;
    AE::Decorator<int> jet_rank;
    AE::Decorator<int> n_primary_vertices;
    AE::Decorator<float> primary_vertex_detector_z;
    AE::Decorator<float> primary_vertex_detector_z_uncertainty;
    Decorators():
      jvt("bTagJVT"),
      fcal_et_tev("FCal_Et_TeV"),
      jet_rank("jetPtRank"),
      n_primary_vertices("nPrimaryVertices"),
      primary_vertex_detector_z("primaryVertexDetectorZ"),
      primary_vertex_detector_z_uncertainty(
        "primaryVertexDetectorZUncertainty")
      {}
  };

  SingleBTagTools(const SingleBTagConfig&);
  JetCalibrationTool calibration_tool;
  JetCleaningTool cleaning_tool;
  JetVertexTaggerTool jvttool;

  BJetShallowCopier shallow_copier;
  BTagJetAugmenter jet_augmenter;
  FlavorTagDiscriminants::BTagMuonAugmenter muon_augmenter;

  std::vector<FlavorTagDiscriminants::DL2HighLevel> dl2s;

  Accessors acc;
  Decorators dec;

  std::optional<JetTruthDecorator> hadrons;
  std::optional<JetTruthDecorator> leptons;

  TrackTruthDecorator trkTruthDecorator;
  TrackVertexDecorator trkVertexDecorator;
  TrackAmbiDecorator trkAmbiDecorator;
  DecoratorExample example_decorator;

  HitDecorator hit_decorator;

};

struct SingleBTagOutputs {
  SingleBTagOutputs(const SingleBTagConfig&, H5::H5File&);

  BTagJetWriter jet_writer;
  std::vector<TrackToolkit> tracks;
  std::vector<TruthWriter> truths;
  std::unique_ptr<HitWriter> hits;

  TruthCorruptionCounter truth_counts;

};

#endif
