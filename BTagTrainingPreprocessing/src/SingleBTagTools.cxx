#include "SingleBTagTools.hh"

#include "SingleBTagConfig.hh"
#include "BTagJetWriterConfig.hh"
#include "BTagTrackWriterConfig.hh"

#include "H5Cpp.h"

namespace {
  std::vector<std::string> get(const VariableList& v, const std::string& k) {
    if (v.count(k)) return v.at(k);
    return {};
  }
  void check_rc(StatusCode code) {
    if (!code.isSuccess()) throw std::runtime_error("bad return code");
  }


  BTagJetWriterConfig jwConfig(const SingleBTagConfig& jobcfg) {
    BTagJetWriterConfig jet_cfg;
    jet_cfg.event_info = get(jobcfg.btag, "event");
    jet_cfg.char_variables = get(jobcfg.btag, "chars");
    jet_cfg.jet_int_variables = get(jobcfg.btag, "jet_int_variables");
    jet_cfg.jet_float_variables = get(jobcfg.btag,"jet_floats");
    jet_cfg.int_as_float_variables = get(jobcfg.btag, "ints_as_float");
    jet_cfg.float_variables = get(jobcfg.btag, "floats");
    jet_cfg.double_variables = get(jobcfg.btag, "doubles");
    jet_cfg.variable_maps.replace_with_defaults_checks
      = jobcfg.default_flag_mapping;
    jet_cfg.variable_maps.rename = {}; // please don't use this :(
    jet_cfg.n_jets_per_event = jobcfg.n_jets_per_event;
    jet_cfg.name = "jets";
    jet_cfg.btagging_link = jobcfg.btagging_link;
    return jet_cfg;
  }
  BTagTrackWriterConfig trackWriterConfig(const TrackConfig& jobcfg) {
    BTagTrackWriterConfig track_cfg;
    track_cfg.name = jobcfg.output_name;
    track_cfg.ip_prefix = jobcfg.ip_prefix;
    track_cfg.variables = jobcfg.variables;
    track_cfg.output_size = {jobcfg.n_to_save};
    return track_cfg;
  }
}

TrackToolkit::TrackToolkit(const TrackConfig& cfg, H5::H5File& output):
  selector(cfg.selection, cfg.input_name),
  sort(trackSort(cfg.sort_order)),
  writer(output, trackWriterConfig(cfg)),
  track_accessor(cfg.ip_prefix),
  n_tracks_decorator("n_" + cfg.output_name)
{
}
TrackToolkit::TrackToolkit(TrackToolkit&&) = default;


SingleBTagTools::Accessors::Accessors(const SingleBTagConfig& cfg):
  eventClean_looseBad("DFCommonJets_eventClean_LooseBad"),
  btaggingLink(cfg.btagging_link)
{}

SingleBTagTools::SingleBTagTools(const SingleBTagConfig& jobcfg):
  calibration_tool("JetCalibrationTool"),
  cleaning_tool("JetCleaningTool", JetCleaningTool::LooseBad, false),
#ifndef DISABLE_JVT
  jvttool("JetVertexTaggerTool"),
#endif
  shallow_copier(jobcfg.btagging_link),
  muon_augmenter("Muons"),
  acc(jobcfg)
{
  if (jobcfg.do_calibration){
    JetCalibrationTool& jct = calibration_tool;
    check_rc( jct.setProperty("JetCollection",
                              jobcfg.jet_calibration_collection) );
    check_rc( jct.setProperty("ConfigFile", jobcfg.jet_calib_file) );
    check_rc( jct.setProperty("CalibSequence", jobcfg.cal_seq) );
    check_rc( jct.setProperty("CalibArea", jobcfg.cal_area) );
    check_rc( jct.setProperty("IsData", false) );
    check_rc( jct.initialize() );
  }
  check_rc( cleaning_tool.setProperty("JetContainer",
                                      jobcfg.jet_collection) );
  check_rc( cleaning_tool.initialize() );

  check_rc( jvttool.setProperty("JetContainer",
                                jobcfg.jet_collection) );
  check_rc( jvttool.initialize() );

  for (const auto& cfg: jobcfg.dl2_configs) {
    using FlavorTagDiscriminants::FlipTagConfig;
    std::string path = cfg.nn_file_path;
    std::cout << "loading " << path << std::endl;
    dl2s.emplace_back(path, FlipTagConfig::STANDARD, cfg.remapping);
  }

  // instantiate truth particle decorators
  for (const auto& cfg: jobcfg.truths) {
    if ( cfg.output_name == "hadrons" ) {
      hadrons = JetTruthDecorator(cfg.output_name, cfg.selection);
    }
    else if ( cfg.output_name == "leptons" ) {
      leptons = JetTruthDecorator(cfg.output_name, cfg.selection);
    }
  }
}

SingleBTagOutputs::SingleBTagOutputs(const SingleBTagConfig& jobcfg,
                                     H5::H5File& output):
  jet_writer(output, jwConfig(jobcfg))
{
  for (const TrackConfig& cfg: jobcfg.tracks) {
    tracks.emplace_back(cfg, output);
  }
  for (const TruthConfig& cfg: jobcfg.truths) {
    truths.emplace_back(output, cfg.n_to_save, cfg.output_name, cfg.sort_order);
  }
  if (jobcfg.hits.output_size > 0) {
    hits.reset(new HitWriter(output, jobcfg.hits));
  }

}
