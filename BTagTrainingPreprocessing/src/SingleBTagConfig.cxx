#include "SingleBTagConfig.hh"
#include "ConfigFileTools.hh"
#define BOOST_BIND_GLOBAL_PLACEHOLDERS // ignore deprecated ptree issues
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/filesystem.hpp>

namespace {

  TrackSortOrder get_track_sort_order(const boost::property_tree::ptree& pt,
                                const std::string& key) {
    std::string val = pt.get<std::string>(key);
    if (val == "abs_d0_significance") {
      return TrackSortOrder::ABS_D0_SIGNIFICANCE;
    }
    if (val == "abs_d0") {
      return TrackSortOrder::ABS_D0;
    }
    if (val == "d0_significance") {
      return TrackSortOrder::D0_SIGNIFICANCE;
    }
    if (val == "abs_beamspot_d0") {
      return TrackSortOrder::ABS_BEAMSPOT_D0;
    }
    throw std::logic_error("sort order '" + val + "' not recognized");
  }

  TrackConfig get_track_config(const boost::property_tree::ptree& pt) {
    namespace cft = ConfigFileTools;
    TrackConfig cfg;

    cfg.n_to_save = pt.get<size_t>("n_to_save");
    cfg.sort_order = get_track_sort_order(pt,"sort_order");

    const boost::property_tree::ptree& tracksel = pt.get_child("selection");
    TrackSelectorConfig::Cuts& cuts = cfg.selection.cuts;
    cuts.pt_minumum        = cft::null_as_nan(tracksel,"pt_minimum");
    cuts.abs_eta_maximum   = cft::null_as_nan(tracksel,"abs_eta_maximum");
    cuts.d0_maximum        = cft::null_as_nan(tracksel,"d0_maximum");
    cuts.z0_maximum        = cft::null_as_nan(tracksel,"z0_maximum");
    cuts.si_hits_minimum   = tracksel.get<int>("si_hits_minimum");
    cuts.si_shared_maximum = tracksel.get<int>("si_shared_maximum");
    cuts.si_holes_maximum  = tracksel.get<int>("si_holes_maximum");
    cuts.pix_holes_maximum = tracksel.get<int>("pix_holes_maximum");

    cfg.selection.btagging_link = pt.get<std::string>("btagging_link");

    cfg.variables = cft::get_track_variables(pt.get_child("variables"));

    cfg.input_name = pt.get<std::string>("input_name");
    cfg.output_name = pt.get<std::string>("output_name");
    cfg.ip_prefix = pt.get<std::string>("ip_prefix");

    // use the same prefix for selection and everything else
    cfg.selection.ip_prefix = cfg.ip_prefix;

    return cfg;
  }
  
  HitWriterConfig get_hit_config(const boost::property_tree::ptree& pt) {
    namespace cft = ConfigFileTools;
    HitWriterConfig cfg;

    cfg.output_size = pt.get<size_t>("n_to_save");
    cfg.name = pt.get<std::string>("output_name");
    cfg.dR_to_jet = pt.get<float>("dR_to_jet");

    return cfg;
  }

  TrackSortOrder get_truth_sort_order(const boost::property_tree::ptree& pt,
                                const std::string& key) {
    std::string val = pt.get<std::string>(key);
    if (val == "pt") {
      return TrackSortOrder::PT;
    }
    throw std::logic_error("sort order '" + val + "' not recognized");
  }

  TruthConfig get_truth_config(const boost::property_tree::ptree& pt) {
    namespace cft = ConfigFileTools;
    TruthConfig cfg;

    cfg.n_to_save = pt.get<size_t>("n_to_save");
    cfg.sort_order = get_truth_sort_order(pt,"sort_order");
    cfg.output_name = pt.get<std::string>("particles");

    return cfg;
  }

  DecorateConfig get_decoration_config(const boost::property_tree::ptree& pt) {
    namespace cft = ConfigFileTools;
    DecorateConfig cfg;

    cfg.jet_aug = cft::boolinate(pt, "jet_aug");
    cfg.btag_jes = cft::boolinate(pt, "btag_jes");
    cfg.soft_muon = cft::boolinate(pt, "soft_muon");
    cfg.track_truth_info = cft::boolinate(pt, "track_truth_info");
    cfg.track_sv_info = cft::boolinate(pt, "track_sv_info");
    cfg.track_ambiguity_solver_ranking = cft::boolinate(pt, "track_ambiguity_solver_ranking");
    cfg.hits = cft::boolinate(pt, "hits");
    return cfg;
  }
}

SingleBTagConfig get_singlebtag_config(const std::string& config_file_name) {
  namespace fs = boost::filesystem;
  namespace pt = boost::property_tree;
  namespace cft = ConfigFileTools;
  SingleBTagConfig config;

  pt::ptree cfg;
  pt::read_json(config_file_name, cfg);

  // combine variable files if there are any file paths specified
  cft::combine_files(cfg, fs::path(config_file_name));

  config.do_heavyions = cft::boolinate(cfg, "do_heavyions");

  config.do_calibration = cft::boolinate(cfg, "do_calibration");
  if (config.do_calibration) {
    config.jet_calibration_collection = cfg.get<std::string>(
      "jet_calibration_collection");
  }
  config.jet_collection = cfg.get<std::string>("jet_collection");

  if(config.do_calibration){
    config.jet_calib_file = cfg.get<std::string>("jet_calib_file");
    config.cal_seq = cfg.get<std::string>("cal_seq");
    config.cal_area = cfg.get<std::string>("cal_area");
    config.jvt_cut = cfg.get<float>("jvt_cut");
  }

  config.pt_cut = cfg.get<float>("pt_cut");

  std::map<std::string, JetCleanOption> jetclean_option_map = { {"none", JetCleanOption::none}, {"event", JetCleanOption::event}, {"jet", JetCleanOption::jet} };
  config.jetclean_option = jetclean_option_map.at(cfg.get<std::string>("jetclean_option"));

  if (config.jetclean_option == JetCleanOption::jet && config.jet_collection == "AntiKt4EMPFlowJets")
    throw std::runtime_error("Must not use individual jet cleaning for AntiKt4EMPFlowJets. Use >jetclean_option: event< instead.");

  config.vertex_collection = cfg.get<std::string>("vertex_collection");
  config.btagging_link = cfg.get<std::string>("btagging_link");

  for (const auto& trkpt: cfg.get_child("tracks")) {
    config.tracks.push_back(get_track_config(trkpt.second));
  }
  
  if (const auto& hitpt = cfg.get_child_optional("hits")) {  
    config.hits = get_hit_config(*hitpt);
  }

  for (const auto& trthpt: cfg.get_child("truths")) {
    config.truths.push_back(get_truth_config(trthpt.second));
  }

  config.n_jets_per_event = cfg.get<size_t>("n_jets_per_event");

  // optional configuration
  for (const auto& nnpt: cfg.get_child("dl2_configs")) {
    config.dl2_configs.push_back(cft::get_dl2_config(nnpt.second));
  }

  config.btag = cft::get_variable_list(cfg.get_child("variables.btag"));
  config.default_flag_mapping = cft::check_map_from(
    cft::get_variable_list(cfg.get_child("variables.default_mapping")));

  config.decorate = get_decoration_config(cfg.get_child("decorate"));

  return config;
}
