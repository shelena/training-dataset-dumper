#include "JetTruthDecorator.hh"

#include "TruthTools.hh"

#include "xAODBTagging/BTaggingUtilities.h"
#include "xAODJet/Jet.h"
#include "xAODTruth/TruthParticle.h"
#include "xAODTruth/TruthParticleContainer.h"


// the constructor just builds the decorator
JetTruthDecorator::JetTruthDecorator(const std::string& link_name,
                                     TruthSelectorConfig config):
  m_deco(link_name),
  m_truth_select_cfg(config),
  m_out_name(link_name)
{
}

// this call actually does the work on the jet
void JetTruthDecorator::decorate(const xAOD::Jet& jet,
                                 const xAOD::TruthParticleContainer* tpc) const 
{
  TruthLinks links;
  for ( const auto part : *tpc ) {
    if ( not passed_cuts(*part, jet) ) {
      continue;
    }
    TruthLink link(*tpc, part->index());
    links.push_back(link);
  }
  m_deco(jet) = links;
}

// selections
bool JetTruthDecorator::passed_cuts(const xAOD::TruthParticle& part,
                                    const xAOD::Jet& jet) const
{

  if ( part.pt() < m_truth_select_cfg.pt_minumum )                 { return false; }
  if ( std::abs(part.eta()) > m_truth_select_cfg.abs_eta_maximum ) { return false; }
  if (jet.p4().DeltaR(part.p4()) > m_truth_select_cfg.dr_maximum ) { return false; }

  if ( m_out_name == "hadrons" ) {
    if ( not truth::isWeaklyDecayingHadron(part) ) { return false; }  
  }
  else if ( m_out_name == "leptons" ) {
    if ( not truth::isFinalStateLepton(part) ) { return false; }  
  }
  else {
    throw std::logic_error("unsupported truth collection");
  }
  
  return true;
}
