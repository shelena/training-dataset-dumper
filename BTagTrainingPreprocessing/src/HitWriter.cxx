#include "HitWriter.hh"
#include "HitDecorator.hh"
#include "HitWriterConfig.hh"
#include "HDF5Utils/Writer.h"

// Less Standard Libraries (for atlas)
#include "H5Cpp.h"

#include "xAODTracking/Vertex.h"

/////////////////////////////////////////////
// Internal classes
/////////////////////////////////////////////

typedef std::function<float(const HitOutputs&)> FloatFiller;
class HitConsumers: public H5Utils::Consumers<const HitOutputs&> {};
class HitOutputWriter: public H5Utils::Writer<1,const HitOutputs&>
{
public:
  HitOutputWriter(H5::Group& file,
                  const std::string& name,
                  const HitConsumers& cons,
                  size_t size):
    H5Utils::Writer<1,const HitOutputs&>(file, name, cons, {{size}}) {}
};


///////////////////////////////////
// Class definition starts here
///////////////////////////////////
HitWriter::HitWriter(
  H5::Group& output_file,
  const HitWriterConfig& config):
  m_hdf5_hit_writer(nullptr)
{

  HitConsumers fillers;
  add_hit_variables(fillers);

  // build the output dataset
  if (config.name.size() == 0) {
    throw std::logic_error("hit output name not specified");
  }
  if (config.output_size == 0) {
    throw std::logic_error("can't make an output writer with no hits");
  }
  m_dR_to_jet = config.dR_to_jet;

  m_hdf5_hit_writer.reset(
    new HitOutputWriter(
      output_file, config.name, fillers, config.output_size));
}

HitWriter::~HitWriter() {
  if (m_hdf5_hit_writer) m_hdf5_hit_writer->flush();
}

HitWriter::HitWriter(HitWriter&&) = default;

void HitWriter::write(const xAOD::TrackMeasurementValidationContainer& hits,
                      const xAOD::Jet& jet,
		      const xAOD::Vertex& vx) {
  if (m_hdf5_hit_writer) {
    std::vector<HitOutputs> hit_outputs;
    for (const auto hit: hits) {
      // Only write out the hit if it is within a given dR from a jet
      TVector3 hitPos(hit->globalX() - vx.x(), hit->globalY() - vx.y(), hit->globalZ() - vx.z());
      if (jet.p4().Vect().DeltaR(hitPos) < m_dR_to_jet) {
        hit_outputs.push_back(HitOutputs{hit, &jet, &vx});
      }
    }

    m_hdf5_hit_writer->fill(hit_outputs);
  }
}

void HitWriter::write_dummy() {
  if (m_hdf5_hit_writer) {
    std::vector<HitOutputs> hit_outputs;
    m_hdf5_hit_writer->fill(hit_outputs);
  }
}

void HitWriter::add_hit_variables(HitConsumers& vars) {

  vars.add("x_local", [](const HitOutputs& h){ return h.hit->globalX() - h.pv->x(); }, NAN);
  vars.add("y_local", [](const HitOutputs& h){ return h.hit->globalY() - h.pv->y(); }, NAN);
  vars.add("z_local", [](const HitOutputs& h){ return h.hit->globalZ() - h.pv->z(); }, NAN);

}
