#include "BTagJetWriterUtils.hh"
#include "BTagJetWriterConfig.hh"

#include "HDF5Utils/Writer.h"

#include "xAODJet/Jet.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODBTagging/BTaggingUtilities.h"

#include <regex>

namespace {
  void extract_and_add_float(JetConsumers& fillers,
                             std::vector<std::string>& var_list,
                             std::function<float(const xAOD::Jet&)> fillfunc,
                             const std::string& name) {
    auto itr = std::find(var_list.begin(), var_list.end(), name);
    if (itr == var_list.end()) return;

    std::function filler = [fillfunc](const JetOutputs& jo) -> float {
      if (!jo.jet) return NAN;
      return fillfunc(*jo.jet);
    };
    fillers.add(name, filler);
    var_list.erase(itr);
  }
}

JetOutputs::JetOutputs():
  jet(nullptr),
  parent(nullptr),
  event_info(nullptr)
{}

template<typename I, typename O = I>
void add_btag_fillers(JetConsumers&,
                      const std::vector<std::string>&,
                      const BTagVariableMaps&,
                      O default_value,
                      const std::string& btag_link);

template<typename I, typename O = I>
void add_jet_fillers(JetConsumers&,
                     const std::vector<std::string>&,
                     O default_value = O());

void add_jet_variables(JetConsumers& fillers,
                       const BTagJetWriterBaseConfig& cfg) {
  if ( (cfg.double_variables.size()
        + cfg.float_variables.size()
        + cfg.int_variables.size() ) > 0) {
    // in this case we convert doubles to floats to save space,
    // note that you can change the second parameter to a double
    // to get full precision.
    const BTagVariableMaps& m = cfg.variable_maps;
    const std::string& l = cfg.btagging_link;
    add_btag_fillers<double, float>(fillers, cfg.double_variables, m, NAN, l);
    add_btag_fillers<float>(fillers, cfg.float_variables, m, NAN, l);
    add_btag_fillers<int>(fillers, cfg.int_variables, m, -1, l);
    add_btag_fillers<char>(fillers, cfg.char_variables, m, -1, l);
    add_btag_fillers<int, float>(
      fillers, cfg.int_as_float_variables, m, NAN, l);
  }

  std::vector<std::string> unused_jet_floats(
    cfg.jet_float_variables.begin(),
    cfg.jet_float_variables.end());
  extract_and_add_float(
    fillers, unused_jet_floats,
    [](const auto& j){ return j.pt(); }, "pt");
  extract_and_add_float(
    fillers, unused_jet_floats,
    [](const auto& j){ return j.eta(); }, "eta");
  extract_and_add_float(
    fillers, unused_jet_floats,
    [](const auto& j){ return j.e(); }, "energy");
  extract_and_add_float(
    fillers, unused_jet_floats,
    [](const auto& j){ return j.m(); }, "mass");
  if (unused_jet_floats.size() > 0) {
    add_jet_fillers<float>(fillers, unused_jet_floats, NAN);
  }

  add_jet_int_variables(fillers, cfg.jet_int_variables);

}


void add_jet_int_variables(JetConsumers& vars,
                                     const std::vector<std::string>& names) {
  for (const auto& label_name: names) {
    // accomidate some custom cases for jet ints
    if (label_name == "numConstituents") {
      auto f = [](const JetOutputs& jo) -> int {
                 if (!jo.jet) return -1;
                 return jo.jet->numConstituents();
               };
      vars.add<int>(label_name, f);
    } else {
      // if nothing else works, we end up here
      SG::AuxElement::ConstAccessor<int> lab(label_name);
      std::function<int(const JetOutputs&)> filler
        = [lab](const JetOutputs& jo) {
            if (!jo.jet) return -1;
            return lab(*jo.jet);
          };
      vars.add(label_name, filler);
    }
  }
}
void add_parent_jet_int_variables(
  JetConsumers& vars,
  const std::vector<std::string>& names) {
  for (const auto& label_name: names) {
    SG::AuxElement::ConstAccessor<int> lab(label_name);
    std::function<int(const JetOutputs&)> filler = [
      lab](const JetOutputs& jo) {
      if (!jo.parent) return -1;
      return lab(*jo.parent);
    };
    vars.add(label_name, filler);
  }
}

void add_parent_fillers(JetConsumers& vars) {
  FloatFiller deta = [](const JetOutputs& jo) -> float {
    if (!jo.jet || !jo.parent) return NAN;
    return jo.jet->eta() - jo.parent->eta();
  };
  vars.add("deta", deta);
  FloatFiller dphi = [](const JetOutputs& jo) -> float {
    if (!jo.jet || !jo.parent) return NAN;
    return jo.jet->p4().DeltaPhi(jo.parent->p4());
  };
  vars.add("dphi", dphi);
  FloatFiller dr = [](const JetOutputs& jo) -> float {
    if (!jo.jet || !jo.parent) return NAN;
    return jo.jet->p4().DeltaR(jo.parent->p4());
  };
  vars.add("dr", dr);
}

void add_event_info(
  JetConsumers& vars,
  const std::vector<std::string>& keys
  ) {
  std::function<float(const JetOutputs&)> evt_weight = [](const JetOutputs& jo) {
    if (!jo.event_info) throw std::logic_error("missing event info");
    return jo.event_info->mcEventWeight();
  };
  std::function<long long(const JetOutputs&)> evt_number = [](const JetOutputs& jo) {
    if (!jo.event_info) throw std::logic_error("missing event info");
    return jo.event_info->eventNumber();
  };

  std::function<float(const JetOutputs&)> evt_mu = [](const JetOutputs& jo) {
    if (!jo.event_info) throw std::logic_error("missing event info");
    return jo.event_info->averageInteractionsPerCrossing();
  };

  std::function<float(const JetOutputs&)> evt_act_mu = [](const JetOutputs& jo) {
    if (!jo.event_info) throw std::logic_error("missing event info");
    return jo.event_info->actualInteractionsPerCrossing();
  };
  std::regex int_type("^n[A-Z].*");

  for (const auto& key: keys) {
    if (key == "mcEventWeight") {
      vars.add(key, evt_weight);
    } else if (key == "eventNumber") {
      vars.add(key, evt_number);
    } else if (key == "averageInteractionsPerCrossing") {
      vars.add(key, evt_mu);
    } else if (key == "actualInteractionsPerCrossing") {
      vars.add(key, evt_act_mu);
    } else if (std::regex_match(key, int_type)) {
      using IA = SG::AuxElement::ConstAccessor<int>;
      std::function getter([g=IA(key)] (const JetOutputs& jo) {
        return g(*jo.event_info);
      });
      vars.add(key, getter);
    } else {
      // let's just pretend everything else is a float
      using FA = SG::AuxElement::ConstAccessor<float>;
      std::function getter([g=FA(key)] (const JetOutputs& jo) {
        return g(*jo.event_info);
      });
      vars.add(key, getter);
    }
  }
}



JetOutputWriter::JetOutputWriter(H5::Group& file,
                                 const std::string& name,
                                 const JetConsumers& cons):
  m_writer(file, name, cons)
{
}
void JetOutputWriter::fill(const std::vector<JetOutputs>& jos) {
  for (const auto& jo: jos) {
    m_writer.fill(jo);
  }
}
void JetOutputWriter::flush() {
  m_writer.flush();
}


JetOutputWriter1D::JetOutputWriter1D(H5::Group& file,
                                     const std::string& name,
                                     const JetConsumers& cons,
                                     size_t n_jets):
  m_writer(file, name, cons, {n_jets})
{
}
void JetOutputWriter1D::fill(const std::vector<JetOutputs>& jos) {
  m_writer.fill(jos);
}
void JetOutputWriter1D::flush() {
  m_writer.flush();
}


// template implementation

template<typename I, typename O>
void add_btag_fillers(
  JetConsumers& vars,
  const std::vector<std::string>& names,
  const BTagVariableMaps& maps,
  O default_value,
  const std::string& btagging_link) {
  const auto& def_check_names = maps.replace_with_defaults_checks;
  SG::AuxElement::ConstAccessor<ElementLink<xAOD::BTaggingContainer>> blink(
    btagging_link);
  for (const auto& btag_var: names) {
    SG::AuxElement::ConstAccessor<I> getter(btag_var);
    std::string out_name = btag_var;
    if (maps.rename.count(btag_var)) out_name = maps.rename.at(btag_var);
    if (def_check_names.count(btag_var)) {
      SG::AuxElement::ConstAccessor<char> def_check(
        def_check_names.at(btag_var));
      std::function<O(const JetOutputs&)> filler = [
        getter, default_value, def_check, blink](const JetOutputs& jo) -> O {
        if (!jo.jet) return default_value;
        const xAOD::BTagging* btagging = *blink(*jo.jet);
        if (!btagging) {
          throw std::logic_error("missing b-tagging container");
        }
        if (def_check(*btagging)) return default_value;
        return getter(*btagging);
      };
      vars.add(out_name, filler);
    } else {
      std::function<O(const JetOutputs&)> filler = [
        getter, default_value, blink](const JetOutputs& jo) -> O {
        if (!jo.jet) return default_value;
        const xAOD::BTagging* btagging = *blink(*jo.jet);
        if (!btagging) {
          throw std::logic_error("missing b-tagging container");
        }
        return getter(*btagging);
      };
      vars.add(out_name, filler);
    }
  }
}
template<typename I, typename O>
void add_jet_fillers(JetConsumers& vars,
                     const std::vector<std::string>& names,
                     O default_value) {
  for (const auto& btag_var: names) {
    SG::AuxElement::ConstAccessor<I> getter(btag_var);
    std::function<O(const JetOutputs&)> filler = [
      getter, default_value](const JetOutputs& jo) -> O {
      if (!jo.jet) return default_value;
      return getter(*jo.jet);
    };
    vars.add(btag_var, filler);
  }
}

void add_jet_float_fillers(JetConsumers& c,
                           const std::vector<std::string>& n,
                           float default_value) {
  add_jet_fillers<float>(c, n, default_value);
}

