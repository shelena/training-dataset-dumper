#ifndef CONFIG_FILE_TOOLS_HH
#define CONFIG_FILE_TOOLS_HH

#include "DL2Config.hh"

#include <boost/property_tree/ptree.hpp>
#include <boost/filesystem.hpp>
#include <string>
#include <vector>
#include <map>

class TrackWriterVariables;

namespace ConfigFileTools {

  using boost::property_tree::ptree;

  typedef std::map<std::string,std::vector<std::string>> MapOfLists;

  MapOfLists get_variable_list(const boost::property_tree::ptree& pt);
  bool boolinate(const boost::property_tree::ptree& pt,
                 const std::string& key);
  bool boolinate_default(const boost::property_tree::ptree& pt,
                         const std::string& key,
                         bool default_value = false);
  void combine_files(boost::property_tree::ptree& node,
                     boost::filesystem::path config_path);
  float null_as_nan(const boost::property_tree::ptree& node,
                    const std::string& key);

  DL2Config get_dl2_config(const boost::property_tree::ptree& pt);

  std::map<std::string, std::string> check_map_from(const MapOfLists&);

  TrackWriterVariables get_track_variables(const ptree& pt);
}

#endif
