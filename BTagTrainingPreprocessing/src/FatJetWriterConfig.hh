#ifndef FAT_JET_WRITER_CONFIG_HH
#define FAT_JET_WRITER_CONFIG_HH

#include <map>
#include <string>
#include <vector>

struct FatJetWriterConfig {
  FatJetWriterConfig();

  std::string name;

  std::vector<std::string> event_info;
  bool write_kinematics_relative_to_parent;
  bool write_substructure_moments;

  std::vector<std::string> jet_int_variables;
  std::vector<std::string> jet_float_variables;

  std::vector<std::string> parent_jet_int_variables;
};


#endif
