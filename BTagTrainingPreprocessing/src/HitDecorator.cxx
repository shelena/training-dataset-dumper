#include "HitDecorator.hh"

#include "GeoPrimitives/GeoPrimitives.h"
#include "xAODJet/Jet.h"
#include "xAODTracking/TrackMeasurementValidationContainer.h"
#include "xAODTracking/Vertex.h"

namespace {

  // couple functions to get 3 vectors from various places
  Amg::Vector3D get3Vector(const xAOD::TrackMeasurementValidation& h) {
    return {h.globalX(), h.globalY(), h.globalZ()};
  }

  Amg::Vector3D getShifted3Vector(
    const xAOD::TrackMeasurementValidation& h,
    const xAOD::Vertex& v) {
    Amg::Vector3D p = v.position();
    return get3Vector(h) - p;
  }

  // this tries to figure out the pixel layer
  int layerFromGlobal(const Amg::Vector3D& v) {
    // stolen from spyros:
    //
    // https://gitlab.cern.ch/atlas-flavor-tagging-tools/highpt-trackless-btagging/telanalysis/-/blob/1a65cb8a587b9073fef4493dc916c5adc95bb7d2/TELAnalysis/Root/Hit.cxx#L21
    //
    // Thanks spyros!
    //
    float r2d = v.perp();
    float z = v.z();
    if (r2d > 30 && r2d < 40) {
      if (std::abs(z) < 331.5) return 0;
    } else if (r2d > 44 && r2d < 58) {
      if (std::abs(z) < 400.5) return 1;
    } else if (r2d > 83 && r2d < 95) {
      if (std::abs(z) < 400.5) return 2;
    } else if (r2d > 115 && r2d < 130) {
      if (std::abs(z) < 400.5) return 3;
    }
    return -1;
  }

}


// you have to give the decorators their names here
HitDecorator::HitDecorator():
  m_nHits_L0("nHits_L0"),
  m_nHits_L1("nHits_L1"),
  m_nHits_L2("nHits_L2"),
  m_nHits_L3("nHits_L3")
{
}

// the main function you'll call
void HitDecorator::decorate(
  const xAOD::Jet& jet,
  const xAOD::TrackMeasurementValidationContainer& hits,
  const xAOD::Vertex& vertex) const
{

  //const int hit_size = hits.size();
  //float hit_x[hit_size];

  // loop over all the hits and save the ones that are within some
  // radius
  int nHitsL0(0), nHitsL1(0), nHitsL2(0), nHitsL3(0);
  int count(0);
  for (const auto* raw_hit: hits) {
    TVector3 local(getShifted3Vector(*raw_hit, vertex).data());
    if (jet.p4().Vect().DeltaR(local) < 0.04) {
      int layer = layerFromGlobal(get3Vector(*raw_hit));
      if (layer == 0) ++nHitsL0;
      else if (layer == 1) ++nHitsL1;
      else if (layer == 2) ++nHitsL2;
      else if (layer == 3) ++nHitsL3;
    }
    ++count;
  }
  // decorate the jet with something
  m_nHits_L0(jet) = nHitsL0;
  m_nHits_L1(jet) = nHitsL1;
  m_nHits_L2(jet) = nHitsL2;
  m_nHits_L3(jet) = nHitsL3;

}
