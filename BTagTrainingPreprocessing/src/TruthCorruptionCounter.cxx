#include "TruthCorruptionCounter.hh"
#include "TruthTools.hh"

#include "xAODEventInfo/EventInfo.h"

#include "nlohmann/json.hpp"

#include <fstream>
#include <sstream>

void writeTruthCorruptionCounts(
  const TruthCorruptionCounter& counts,
  const std::string& output_file) {

  nlohmann::json metadata;
  metadata["n_successful_truth_reads"] = counts.n_successful_truth_reads;
  metadata["n_failed_truth_reads"] = counts.n_failed_truth_reads;
  std::ofstream metastream(output_file);
  metastream << metadata.dump(2);

}

std::string getEventInfoString(
  const truth::TruthRecordError& err,
  const xAOD::EventInfo& event_info) {
  std::stringstream stream;
  stream << "Truth error: '" << err.what() << "'"
         << " skipping event number " << event_info.eventNumber()
         << " in run number " << event_info.runNumber()
         << " with mc event number " << event_info.mcEventNumber();
  return stream.str();
}
