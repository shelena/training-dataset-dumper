#ifndef PROCESS_SINGLE_BTAG_EVENT_HH
#define PROCESS_SINGLE_BTAG_EVENT_HH

class SingleBTagConfig;
class SingleBTagTools;
class SingleBTagOutputs;
namespace xAOD {
  class TEvent;
}

// the joy of "duel use": there doesn't appear to be a common
// baseclass that implements the one method we need in TEvent and
// StoreGate (i.e. `retrieve`). So we use overloads here, templates
// are defined in processSingleBTagEvent.tcc

// FIXME: SingleBTagTools is not const until we merge
// https://gitlab.cern.ch/atlas/athena/-/merge_requests/49253

void processSingleBTagEvent(xAOD::TEvent&,
                            const SingleBTagConfig&,
                            /*const*/ SingleBTagTools&,
                            SingleBTagOutputs&);

// we only build this guy if we're using a Gaudi build
#ifndef XAOD_STANDALONE
class StoreGateSvc;
void processSingleBTagEvent(StoreGateSvc&,
                            const SingleBTagConfig&,
                            /*const*/ SingleBTagTools&,
                            SingleBTagOutputs&);
#else
// for the non-Gaudi build, there's SgTEvent, which is some kind of
// wrapper for TEvent that looks (kind of) like StoreGate
namespace asg {
  class SgTEvent;
}
void processSingleBTagEvent(asg::SgTEvent&,
                            const SingleBTagConfig&,
                            /*const*/ SingleBTagTools&,
                            SingleBTagOutputs&);
#endif  // XAOD_STANDALONE

#endif  // PROCESS_SINGLE_BTAG_EVENT_HH
