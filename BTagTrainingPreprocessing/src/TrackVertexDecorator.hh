#ifndef TRACK_VERTEX_DECORATOR_HH
#define TRACK_VERTEX_DECORATOR_HH

#include "xAODJet/Jet.h"
#include "xAODBTagging/BTaggingUtilities.h"

#include "xAODBTagging/SecVtxHelper.h"
#include "xAODTracking/VertexContainer.h"
#include "xAODBTagging/BTagVertexContainer.h"

#include <string>

// this is a bare-bones example of a class that could be used to
// decorate a track with additional information

class TrackVertexDecorator
{
public:
  TrackVertexDecorator(const std::string& decorator_prefix = "");

  // this is the function that actually does the decoration
  void decorate(const xAOD::TrackParticle& track, const xAOD::BTagging& btag, const xAOD::Vertex& pv) const;

  float get_AMVF_PV_weight( const xAOD::TrackParticle& track, const xAOD::Vertex& pv ) const;

private:
  // All this class does is apply a decoration, so all it needs to do
  // is contain one decorator. We could have also written a function
  // and statically initalized this, but static data in functions is
  // probably best avoided.

  // SV indices
  SG::AuxElement::Decorator<int> m_track_sv1_idx;
  SG::AuxElement::Decorator<int> m_track_jf_idx;

  SG::AuxElement::ConstAccessor<std::vector<ElementLink<xAOD::VertexContainer>>> m_SV1VxAccessor;
  SG::AuxElement::ConstAccessor<std::vector<ElementLink<xAOD::BTagVertexContainer>>> m_jfVxAccessor;

  // AMVF PV weight
  SG::AuxElement::ConstAccessor<std::vector<ElementLink<xAOD::VertexContainer>>> m_AMVFVertices;
  SG::AuxElement::ConstAccessor<std::vector<float>> m_AMVFWeights;

  SG::AuxElement::Decorator<float> m_AMVFWeightPV;

};

#endif
