#include "SingleBTagAlg.h"

#include "SingleBTagTools.hh"
#include "SingleBTagConfig.hh"
#include "processSingleBTagEvent.hh"

#include "H5Cpp.h"

#include "AsgDataHandles/ReadHandle.h"

SingleBTagAlg::SingleBTagAlg(const std::string& name,
                             ISvcLocator* pSvcLocator):
  EL::AnaAlgorithm(name, pSvcLocator),
  m_output_file(nullptr),
  m_tools(nullptr),
  m_outputs(nullptr)
{
  declareProperty("outputFile", m_output_file_name);
  declareProperty("configFileName", m_config_file_name);
  declareProperty("metadataFileName",
                  m_metadata_file_name = "userJobMetadata.json");
}

SingleBTagAlg::~SingleBTagAlg() {
}

  // these are the functions inherited from Algorithm
StatusCode SingleBTagAlg::initialize () {
  auto cfg = get_singlebtag_config(m_config_file_name);
  m_config.reset(new SingleBTagConfig(cfg));
  m_output_file.reset(new H5::H5File(m_output_file_name, H5F_ACC_TRUNC));
  m_tools.reset(new SingleBTagTools(*m_config));
  m_outputs.reset(new SingleBTagOutputs(*m_config, *m_output_file));

  m_jetKey = cfg.jet_collection;
  ATH_CHECK(m_jetKey.initialize());
  ATH_MSG_DEBUG("Initialized SingleBTagAlg");

  return StatusCode::SUCCESS;
}
StatusCode SingleBTagAlg::execute () {

  // dereference a few things for convenience
  auto& event = *evtStore();
  const auto& jobcfg = *m_config;
  auto& tools = *m_tools;
  auto& outputs = *m_outputs;

  // most of the real work happens in this function
  processSingleBTagEvent(event, jobcfg, tools, outputs);

  return StatusCode::SUCCESS;
}
StatusCode SingleBTagAlg::finalize () {

  if (m_metadata_file_name.size() > 0) {
    writeTruthCorruptionCounts(m_outputs->truth_counts, m_metadata_file_name);
  }

  return StatusCode::SUCCESS;
}
