#ifndef DL2_CONFIG_HH
#define DL2_CONFIG_HH

#include <string>
#include <vector>
#include <map>

struct DL2Config {
  std::string nn_file_path;
  std::map<std::string, std::string> remapping;
};

#endif
