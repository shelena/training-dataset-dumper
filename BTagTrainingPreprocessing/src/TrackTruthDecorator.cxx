#include "TrackTruthDecorator.hh"

enum OriginType {
  Pileup=0,
  Fake,
  Primary,
  FromB,
  FromBC,
  FromC,
  FromTau,
  OtherSecondary
};


int get_track_label(int origin) {
  /*
    The origin from the InDetTrackTruthOriginTool has inclusive categories.
    Here we use the origin to build exclusive categories.
  */

  int exclusive_track_label = -2;

  if ( InDet::TrkOrigin::isPileup(origin) ) {
    exclusive_track_label = OriginType::Pileup; // PU / no truth link
  }
  else if ( InDet::TrkOrigin::isFake(origin) ) {
    exclusive_track_label = OriginType::Fake;
  }
  else if ( InDet::TrkOrigin::isFromBNotFromD(origin) ) {
    exclusive_track_label = OriginType::FromB;
  }
  else if ( InDet::TrkOrigin::isFromDfromB(origin) ) {
    exclusive_track_label = OriginType::FromBC;
  }
  else if ( InDet::TrkOrigin::isFromDNotFromB(origin) ) {
    exclusive_track_label = OriginType::FromC;
  }
  else if ( InDet::TrkOrigin::isFromTau(origin) ) {
    exclusive_track_label = OriginType::FromTau;
  }
  else if ( InDet::TrkOrigin::isSecondary(origin) ) {
    exclusive_track_label = OriginType::OtherSecondary;
  }
  else if ( InDet::TrkOrigin::isPrimary(origin) ) {
    exclusive_track_label = OriginType::Primary;
  }

  return exclusive_track_label;
}


float get_dr(const xAOD::TruthVertex* vertex_A, const xAOD::TruthVertex* vertex_B) {
  if ( !vertex_A or !vertex_B ) {
    return 999.0;
  }
  float dx = vertex_A->x() - vertex_B->x();
  float dy = vertex_A->y() - vertex_B->y();
  float dz = vertex_A->z() - vertex_B->z();
  return sqrt( pow(dx, 2) + pow(dy, 2) + pow(dz, 2) );  
}


bool TrackTruthDecorator::sort_tracks(const xAOD::TrackParticle* track_A , const xAOD::TrackParticle* track_B ) {
  return track_A->pt() < track_B->pt();
}

// the constructor just builds the decorator
TrackTruthDecorator::TrackTruthDecorator(const std::string& prefix):
  m_track_origin_label(prefix + "truthOriginLabel"),
  m_track_production_vertex_idx(prefix + "truthVertexIndex"),
  m_InDetTrackTruthOriginTool("InDetTrackTruthOriginTool")
{
  if (!m_InDetTrackTruthOriginTool.initialize()) {
    throw std::runtime_error("Can't initialize InDetTrackTruthOriginTool");
  }
}


// this call actually does the work on the track
void TrackTruthDecorator::decorateAll(TrackSelector::Tracks tracks,
                                      const xAOD::TruthVertex* truth_PV) const {

  // sort the tracks by pt to ensure the vertex clustering is deterministic
  std::sort(tracks.begin(), tracks.end(), sort_tracks);

  // store truth vertex for each track
  auto trk_truth_vertex = std::vector<const xAOD::TruthVertex*>();
  for ( const auto& track : tracks ) {

    // store the truth origin of the track
    int trackTruthOrigin = m_InDetTrackTruthOriginTool.getTrackOrigin( track );
    int trackTruthLabel  = get_track_label( trackTruthOrigin );
    m_track_origin_label(*track) = trackTruthLabel;

    // get the truth vertex of the track and store for now
    auto truth_vertex = get_truth_vertex( track );
    trk_truth_vertex.push_back(truth_vertex);
  }

  // decorate tracks with truth vertex info
  auto seen_vertices = std::vector<const xAOD::TruthVertex*>();
  for ( size_t i = 0; i != trk_truth_vertex.size(); i++) {
    auto this_vert  = trk_truth_vertex.at(i);
    auto this_track = tracks.at(i);

    // do we have a vertex for this track?
    if ( !this_vert ) {
      m_track_production_vertex_idx(*this_track) = -2;
      continue;
    }    

    // track from PV
    if ( get_dr(this_vert, truth_PV) < min_dr_to_merge ) {
      m_track_production_vertex_idx(*this_track) = 0;
      continue;
    }

    // have we already seen this vertex?
    bool new_vertex = true;
    for ( size_t j = 0; j != seen_vertices.size(); j++) {
      float dr = get_dr(seen_vertices.at(j), this_vert);

      if ( dr < min_dr_to_merge ) {
        // a vertex is nearby, reuse it
        new_vertex = false;
        m_track_production_vertex_idx(*this_track) = j+1;
        break;
      }
    }

    // this vertex is far enough away from others to be considered unique
    if ( new_vertex ) {
      seen_vertices.push_back(this_vert);
      m_track_production_vertex_idx(*this_track) = seen_vertices.size();
    }
  }
}

const xAOD::TruthVertex* TrackTruthDecorator::get_truth_vertex( const xAOD::TrackParticle* track ) const {
  
  // get the truth particle
  const xAOD::TruthParticle* truth = m_InDetTrackTruthOriginTool.getTruth(track);

  // no truth
  if ( not truth ) {
    return nullptr;
  }

  // no vertex
  const xAOD::TruthVertex* truth_vertex = truth->prodVtx();
  if ( not truth_vertex or truth_vertex->perp() > 440.0 ) { 
    return nullptr;
  }

  return truth_vertex;
}


const xAOD::TruthVertex* TrackTruthDecorator::get_nearest_vertex(const xAOD::TruthVertex* search_vertex, 
                                                                 std::vector<const xAOD::TruthVertex*> vertices) const {

  if ( !search_vertex ) {
    return nullptr;
  }

  float min_dr = 1e5;
  int min_dr_idx = -1;
  int index = -1;

  // find closest vertex
  for ( auto vertex : vertices ) {
    index++;
    if ( !vertex or search_vertex == vertex ) {
      continue;
    }
    float dr = get_dr(vertex, search_vertex);
    if ( dr < min_dr ) {
      min_dr = dr;
      min_dr_idx = index;
    }
  }

  // return closest vertex
  if ( min_dr_idx < 0 ) {
    return nullptr;
  } else {
    return vertices.at(min_dr_idx);
  }
}
