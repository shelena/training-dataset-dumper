#ifndef TRACK_SELECTOR_CONFIG_HH
#define TRACK_SELECTOR_CONFIG_HH

#include <string>

struct TrackSelectorConfig
{
  struct Cuts {
    float pt_minumum      = 1e3;
    float abs_eta_maximum = 2.5;
    float d0_maximum      = 1;
    float z0_maximum      = 1.5;
    int si_hits_minimum   = 7;
    int si_shared_maximum = 1;
    int si_holes_maximum  = 2;
    int pix_holes_maximum = 1;
  };
  Cuts cuts;
  std::string ip_prefix = "btagIp_";
  std::string btagging_link = "btaggingLink";
};

#endif
