#include "SubjetWriter.hh"
#include "BTagJetWriterConfig.hh"

#include "BTagJetWriterUtils.hh"
#include "SubstructureAccessors.hh"

#include "HDF5Utils/Writer.h"

// Less Standard Libraries (for atlas)
#include "H5Cpp.h"

// ATLAS things
#include "xAODJet/Jet.h"
#include "xAODEventInfo/EventInfo.h"

SubjetWriter::SubjetWriter(
  H5::Group& output_file,
  const SubjetWriterConfig& config)
{

  // create the variable fillers
  JetConsumers fillers;

  add_jet_variables(fillers, config);
  add_event_info(fillers, config.event_info);

  if (config.write_kinematics_relative_to_parent) {
    add_parent_fillers(fillers);
  }

  m_hdf5_jet_writer.reset(
    new JetOutputWriter(output_file, config.name, fillers));
}

SubjetWriter::~SubjetWriter() {
  if (m_hdf5_jet_writer) m_hdf5_jet_writer->flush();
}
SubjetWriter::SubjetWriter(SubjetWriter&&) = default;


void SubjetWriter::write_dummy() {
  JetOutputs dummy;
  m_hdf5_jet_writer->fill({dummy});
}

void SubjetWriter::write_with_parent(const xAOD::Jet& jet,
                                      const xAOD::Jet& parent,
                                      const xAOD::EventInfo* mc_event_info) {
  JetOutputs jo;
  jo.event_info = mc_event_info;
  jo.jet = &jet;
  jo.parent = &parent;
  m_hdf5_jet_writer->fill({jo});
}

