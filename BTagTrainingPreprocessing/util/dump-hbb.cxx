/// @file dump-test.cxx
/// @brief Test reading the main calo cluster and track particle container
///
/// This test simply reads in the static payload of the track particle
/// container of a primary xAOD. And checks how fast this can actually
/// be done.

#include "HbbOptions.hh"
#include "HbbConfig.hh"

// new way to do local things
#include "BTagTrackWriterConfig.hh"
#include "JetConstWriterConfig.hh"
#include "BTagJetWriterConfig.hh"
#include "FatJetWriterConfig.hh"
#include "SubjetWriter.hh"
#include "FatJetWriter.hh"
#include "BTagTrackWriter.hh"
#include "JetConstWriter.hh"
#include "addMetadata.hh"
#include "TrackSelector.hh"
#include "ConstituentSelector.hh"
#include "TruthTools.hh"
#include "BTagInputChecker.hh"
#include "errorLogger.hh"
#include "streamers.hh"
#include "InDetTrackSelectionTool/InDetTrackSelectionTool.h"

// System include(s):
#include <memory>
#include <string>
#include <regex>

// ROOT include(s):
#include <TFile.h>
#include <TError.h>
#include <TLorentzVector.h>

// HDF includes
#include "H5Cpp.h"

// json
#include "nlohmann/json.hpp"

// AnalysisBase tool include(s):
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/tools/TFileAccessTracer.h"
#include "AsgTools/AnaToolHandle.h"
#include "FlavorTagDiscriminants/BTagJetAugmenter.h"
#include "FlavorTagDiscriminants/BTagTrackIpAccessor.h"
#include "FlavorTagDiscriminants/DL2HighLevel.h"
#include "CutBookkeeperUtils/OriginalAodCounts.h"
#include "PathResolver/PathResolver.h"
#include "xAODBTagging/BTaggingUtilities.h"

// #include "FlavorTagDiscriminants/VRJetOverlapDecorator.h"
#include "FlavorTagDiscriminants/HbbTag.h"

// EDM include(s):
#include "JetCalibTools/JetCalibrationTool.h"
#include "xAODTracking/TrackParticleContainer.h"
#include "xAODPFlow/TrackCaloCluster.h"
#include "xAODJet/JetContainer.h"
#include "xAODTruth/TruthEventContainer.h"


// sort functions
bool by_d0(const xAOD::TrackParticle* t1,
           const xAOD::TrackParticle* t2) {
  static SG::AuxElement::ConstAccessor<float> d0("btagIp_d0");
  return std::abs(d0(*t1)) > std::abs(d0(*t2));
}
bool pt_sort(const xAOD::Jet* sj1, const xAOD::Jet* sj2 ){
  return sj1->pt() > sj2->pt();
}
bool pt_sort_const(const xAOD::TrackCaloCluster* tcc1, const xAOD::TrackCaloCluster* tcc2 ){
  return tcc1->pt() > tcc2->pt();
}
bool dR_sort(const xAOD::Jet* sj1, const xAOD::Jet* sj2){
  static SG::AuxElement::ConstAccessor<float> dR("dR_fatjet");
  return dR(*sj1) < dR(*sj2);
}

struct SubjetCollection {
  SubjetConfig cfg;
  typedef std::vector<ElementLink<xAOD::IParticleContainer> > ParticleLinks;
  SG::AuxElement::ConstAccessor<ParticleLinks> subjet_acc;
  std::vector<FlavorTagDiscriminants::DL2HighLevel> dl2s;
  std::vector<SubjetWriter> jet_writers;
  std::vector<std::unique_ptr<BTagTrackWriter> > track_writers;
  SG::AuxElement::Decorator<int> nSubjets;
  SubjetCollection(const SubjetConfig&);
};
SubjetCollection::SubjetCollection(const SubjetConfig& init_cfg):
  cfg(init_cfg),
  subjet_acc(init_cfg.input_name),
  nSubjets("n" + init_cfg.output_name + "Subjets")
{
  for (const DL2Config& cfg: init_cfg.dl2_configs) {
    dl2s.emplace_back(
      cfg.nn_file_path,
      FlavorTagDiscriminants::FlipTagConfig::STANDARD,
      cfg.remapping);
  }
}

typedef std::map<std::string,std::vector<std::string>> MapOfLists;
std::vector<std::string> get(const MapOfLists& v, const std::string& k) {
  if (v.count(k)) return v.at(k);
  throw std::runtime_error("key '" + k + "' not found in config file");
}

void check_rc(StatusCode code) {
  if (!code.isSuccess()) throw std::runtime_error("bad return code");
}

const double GeV = 1000;

int main (int argc, char *argv[])
{
  using AE = SG::AuxElement;
  namespace ftd = FlavorTagDiscriminants;
  const IOOpts opts = get_io_opts(argc, argv);
  const HbbConfig jobcfg = get_hbb_config(opts.config_file_name);
  // The name of the application:
  static const char *APP_NAME = "BTagTestDumper";

  TrackSelector track_selector;
  BTagJetAugmenter jet_augmenter;
  BTagTrackIpAccessor track_augmenter("btagIp_");
  ConstituentSelector const_selector;

  // Set up the environment:
  check_rc(xAOD::Init());

  // Set up the event object:
  xAOD::TEvent event(xAOD::TEvent::kClassAccess);
  // turn off file reporting since it can hang in some environments
  xAOD::TFileAccessTracer::enableDataSubmission(false);

  // Initialize JetCalibrationTool with release 21 recommendations
  JetCalibrationTool calib_tool("JetCalibrationTool");
  check_rc(calib_tool.setProperty("JetCollection", jobcfg.jet_collection));
  check_rc(calib_tool.setProperty("ConfigFile", jobcfg.jet_calib_file));
  check_rc(calib_tool.setProperty("CalibSequence", jobcfg.cal_seq));
  check_rc(calib_tool.setProperty("CalibArea", jobcfg.cal_area));
  check_rc(calib_tool.setProperty("IsData", false));
  check_rc( calib_tool.initialize() );

  InDet::InDetTrackSelectionTool indettrackselectiontool("InDetTrackSelectionTool", "Loose");
  check_rc( indettrackselectiontool.initialize() );

  // maybe setup a neural network
  std::vector<ftd::HbbTag> taggers;
  for (const std::string& tagpath: jobcfg.hbb_tag_config) {
    ftd::HbbTagConfig cfg(tagpath);
    taggers.emplace_back(cfg);
  }

  // JSSWTopTaggerDNN* top_tagger = nullptr;
  // if (jobcfg.top_tag_config.size() > 0) {
  //   top_tagger = new JSSWTopTaggerDNN("TopTagger");
  //   top_tagger->setProperty("ConfigFile", jobcfg.top_tag_config);
  //   if (!opts.verbose) top_tagger->setProperty("OutputLevel", MSG::ERROR);
  //   top_tagger->initialize();
  // }
  // SG::AuxElement::Decorator<float> dec_top_tagger("JSSTopScore");

  // VR jet overlap
  // VRJetOverlapDecorator vr_jet_overlap_decorator(
  //   VRJetParameters::RHO30MIN02MAX4);

  // define output file output files
  H5::H5File output(opts.out, H5F_ACC_TRUNC);

  H5::Group error_group(output.createGroup("errors"));

  // set up fat jet writer
  FatJetWriterConfig fat_cfg;
  fat_cfg.write_substructure_moments = true;
  fat_cfg.event_info = {
    "mcEventWeight", "eventNumber",
    "averageInteractionsPerCrossing",
    "actualInteractionsPerCrossing",
  };
  fat_cfg.jet_float_variables = get(jobcfg.variables, "floats");
  fat_cfg.parent_jet_int_variables = get(jobcfg.variables, "parent_ints");
  fat_cfg.jet_int_variables = get(jobcfg.variables, "ints");
  fat_cfg.name = "fat_jet";
  FatJetWriter fatjet_writer(output, fat_cfg);

  // Jet constituents
  JetConstWriterConfig const_cfg;
  const_cfg.output_size = {jobcfg.n_const};
  const_cfg.name = "fat_jet_constituents";
  JetConstWriter constituent_writer(output, const_cfg);

  // set up vectors for accessors and subjet writers
  // subjet accessors
  typedef ElementLink<xAOD::JetContainer> JetLink;
  AE::ConstAccessor<JetLink> acc_parent("Parent");

  // constituent accessors
  std::vector<SubjetCollection> subjet_collections;
  for(const auto& cfg: jobcfg.subjet_configs) {

    SubjetWriterConfig jet_cfg;
    jet_cfg.write_kinematics_relative_to_parent = true;
    jet_cfg.double_variables = get(cfg.variables,"doubles");
    jet_cfg.float_variables = get(cfg.variables,"floats");
    jet_cfg.int_variables = get(cfg.variables,"ints");
    jet_cfg.char_variables = get(cfg.variables,"chars");
    jet_cfg.jet_int_variables = get(cfg.variables,"jet_ints");
    jet_cfg.jet_float_variables = get(cfg.variables, "jet_floats");

    subjet_collections.emplace_back(cfg);
    auto& collection = subjet_collections.back();
    for (size_t jetn = 0; jetn < cfg.n_subjets_to_save; jetn++) {
      jet_cfg.name = "subjet_" + cfg.output_name + "_"
        + std::to_string(1 + jetn);
      collection.jet_writers.emplace_back(output, jet_cfg);
    }
    // set up track writer
    if ( cfg.n_tracks > 0 ) {
      BTagTrackWriterConfig track_cfg;
      track_cfg.variables = cfg.track_variables;
      track_cfg.output_size = {cfg.n_tracks};
      for (size_t jetn = 0; jetn < cfg.n_subjets_to_save; jetn++) {
        track_cfg.name = "subjet_" + cfg.output_name + "_"
          + std::to_string(1 + jetn) + "_tracks";
        collection.track_writers.emplace_back(
          new BTagTrackWriter(output, track_cfg));
      }
    }
  }

  // keep track of the events in the AOD
  OriginalAodCounts counts;

  // keep track of n written jets
  unsigned long long n_jets_written = 0;
  unsigned long long n_skipped_files = 0;

  // keep track of broken links
  BTagInputChecker input_checker;
  unsigned long long n_broken_links = 0;

  // Loop over the specified files:
  for (const std::string& file: opts.in) {

    // Open the file:
    std::unique_ptr<TFile> ifile(TFile::Open(file.c_str(), "READ"));
    if ( ! ifile.get() || ifile->IsZombie()) {
      Error( APP_NAME, "Couldn't open file: %s", file.c_str() );
      // sometimes the grid just doesn't return xrood files... ignore
      // this and eat the missing stats.
      if (opts.ignore_xrootd_fail
          && std::regex_match(file, std::regex("root://.*"))) {
        n_skipped_files++;
        continue;
      } else {
        return 1;
      }
    }
    Info( APP_NAME, "Opened file: %s", file.c_str() );

    // Connect the event object to it:
    check_rc( event.readFrom(ifile.get()) );

    // record some metadata
    const unsigned long long entries = event.getEntries();
    if (entries > 0) {
      event.getEntry(0);
      counts += getOriginalAodCounts(event);
    }
    auto error_logger = getLogger(error_group, file);

    // Loop over its events
    for (unsigned long long entry = 0; entry < entries; ++entry) {
      if (opts.n_entries && entry > opts.n_entries) break;

      // Load the event:
      if (event.getEntry(entry) < 0) {
        Error( APP_NAME, "Couldn't load entry %lld from file: %s",
               entry, file.c_str() );
        return 1;
      }

      // Print some status:
      if ( ! (entry % 500)) {
        Info( APP_NAME, "Processing entry %lld / %lld", entry, entries );
      }
      const xAOD::EventInfo* event_info = 0;
      check_rc( event.retrieve(event_info, "EventInfo") );


      const xAOD::TruthParticleContainer *tpc = nullptr;
      check_rc( event.retrieve(
                  tpc, jobcfg.truth_wz_electron_container) );
      std::vector<const xAOD::TruthParticle*> truth_particles(
        tpc->begin(), tpc->end());
      auto truth_leptons = truth::getLeptonsFromWZ(truth_particles);

      const xAOD::JetContainer *raw_jets = 0;
      auto full_collection = jobcfg.jet_collection + "Jets";
      check_rc( event.retrieve(raw_jets, full_collection) );

      for (const xAOD::Jet *raw_jet : *raw_jets) { // loop over large-R jets
        std::unique_ptr<xAOD::Jet> jet(new xAOD::Jet(*raw_jet));
        // TODO: properly calibrate these jets
        // xAOD::Jet* jet_ptr(nullptr);
        // calib_tool.calibratedCopy(*raw_jet, jet_ptr);
        if (jet->pt() < 250*GeV || std::abs(jet->eta()) > 2.0) {
          continue;
        }

        if (truth::is_overlaping_lepton(*jet, truth_leptons, 1.0)) {
          continue;
        }

        for (const auto& tagger: taggers) tagger.decorate(*jet);
        // if (top_tagger) dec_top_tagger(*jet) = top_tagger->getScore(*jet);

        // constituents
        bool save_constituents = jobcfg.n_const > 0;
        if (save_constituents) {
          auto constituents = const_selector.get_constituents(*jet);
          sort(constituents.begin(), constituents.end(), pt_sort_const);
          constituent_writer.write(constituents, *jet);
        }

        // get parent
        const xAOD::Jet* parent_jet = *acc_parent(*jet);
        if (!parent_jet) throw std::logic_error("no valid parent");


        // loop over subjet collections
        for(auto& collection: subjet_collections) {

          std::vector<const xAOD::Jet*> subjets;

          // depending on the jet collection we're looking at, we have
          // to grab the collection from different places
          bool use_parent = collection.cfg.get_subjets_from_parent;
          const xAOD::Jet* subjet_base = use_parent ? parent_jet : jet.get();
          auto subjet_links = collection.subjet_acc(*subjet_base);

          for (const auto& el: subjet_links) {
            const auto* sjet = dynamic_cast<const xAOD::Jet*>(*el);
            if (!sjet) throw std::logic_error("subjet is invalid");
            if (sjet->pt() < collection.cfg.min_jet_pt || std::abs(sjet->eta()) > 2.5) continue;
            if (sjet->numConstituents() < collection.cfg.num_const ) continue;
            subjets.push_back(sjet);
          }

          collection.nSubjets(*jet) = subjets.size();
          std::sort(subjets.begin(), subjets.end(), pt_sort);

          size_t n_writers = collection.jet_writers.size();
          bool save_tracks = collection.cfg.n_tracks > 0;
          for (size_t jet_number = 0; jet_number < n_writers; jet_number++) {
            auto& writer = collection.jet_writers.at(jet_number);
            BTagTrackWriter* track_writer = nullptr;
            if (save_tracks) {
              track_writer = collection.track_writers.at(jet_number).get();
            }

            // check to see if this jet is valid for writing
            bool valid_jet = true;
            if (jet_number >= subjets.size()) {
              valid_jet = false;
            } else if (collection.cfg.skip_if_missing_tracks) {
              auto& subjet = *subjets.at(jet_number);
              if (!input_checker.hasTracks(subjet)) {
                valid_jet = false;
                n_broken_links++;
                unsigned n_missing = input_checker.nMissingTracks(subjet);
                LoggerInputs jetpars{
                  jet.get(), parent_jet, jet_number, n_missing};
                error_logger.fill(jetpars);
              }
            }

            // if it is we write it, if not fill a placeholder
            if (valid_jet) {
              auto& subjet = *subjets.at(jet_number);

              const auto* btag = xAOD::BTaggingUtilities::getBTagging(subjet);
              jet_augmenter.augmentJfDr(*btag);
              jet_augmenter.augmentIpRatios(*btag);
              for (const auto& dl2: collection.dl2s) {
                dl2.decorate(*btag);
              }

              writer.write_with_parent(subjet, *jet);
              n_jets_written++;

              if (track_writer) {
                auto tracks = track_selector.get_tracks(subjet);
                for (const auto& track: tracks) {
                  track_augmenter.augment_with_ip(*track, subjet);
                }
                sort(tracks.begin(), tracks.end(), by_d0);
                track_writer->write(tracks, subjet);
              }
            } else { // invaid jet
              writer.write_dummy();
              if (track_writer) track_writer->write_dummy();
            }
          } // end loop over subjets
        } // end loop over subjet collections

        // we write the fatjet here because some decorations are
        // applied above
        fatjet_writer.write_with_parent(*jet, *parent_jet, event_info);

      } // end jet loop

    } // end event loop
  } // end file loop

  // save count metadata
  addMetadata(output, counts);

  nlohmann::json metadata;
  metadata["n_jets"] = n_jets_written;
  metadata["n_jets_with_broken_track_links"] = n_broken_links;
  if (opts.ignore_xrootd_fail) {
    metadata["n_skipped_files"] = n_skipped_files;
  }
  metadata["n_files"] = opts.in.size();
  std::ofstream metastream("userJobMetadata.json");
  metastream << metadata.dump(2);

  return 0;
}

// helper for gdb
void dump(const std::map<std::string, std::map<std::string, double>>& m){
  for (const auto& p: m) {
    std::cout << p.first << ":" << std::endl;
    for (const auto& ip: p.second) {
      std::cout << "  " << ip.first << " " << ip.second << std::endl;
    }
  }
}
