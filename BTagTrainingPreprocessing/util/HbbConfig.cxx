#include "HbbConfig.hh"
#include "ConfigFileTools.hh"

#define BOOST_BIND_GLOBAL_PLACEHOLDERS // ignore deprecated ptree issues
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/filesystem.hpp>

HbbConfig get_hbb_config(const std::string& config_file_name) {
  namespace cft = ConfigFileTools;
  namespace fs = boost::filesystem;
  using boost::property_tree::ptree;
  HbbConfig config;

  boost::property_tree::ptree pt;
  boost::property_tree::read_json(config_file_name, pt);

  for (auto& subnode: pt.get_child("subjets")) {
    auto& subjet = subnode.second;
    SubjetConfig cfg;
    cfg.input_name = subjet.get<std::string>("input_name");
    cfg.output_name = subjet.get<std::string>("output_name");
    cfg.get_subjets_from_parent = cft::boolinate(
      subjet,"get_subjets_from_parent");
    cfg.n_subjets_to_save = subjet.get<size_t>("n_subjets_to_save");
    cfg.n_tracks = subjet.get<size_t>("n_tracks");
    cfg.min_jet_pt = subjet.get<double>("min_jet_pt");
    cfg.num_const = subjet.get<size_t>("num_const");
    // read in the b-tagging variables
    cft::combine_files(subjet.get_child("variables"),
                       fs::path(config_file_name));
    cfg.variables = cft::get_variable_list(subjet.get_child("variables"));
    cfg.track_variables = cft::get_track_variables(
      pt.get_child("track_variables"));

    const std::string nn_key = "dl2_configs";
    for (const auto& nnpt: subjet.get_child(nn_key)) {
      cfg.dl2_configs.push_back(cft::get_dl2_config(nnpt.second));
    }
    cfg.skip_if_missing_tracks = cft::boolinate_default(
      subjet, "skip_if_missing_tracks");

    config.subjet_configs.push_back(cfg);
  }
  config.jet_collection = pt.get<std::string>("jet_collection");
  config.jet_calib_file = pt.get<std::string>("jet_calib_file");
  config.cal_seq = pt.get<std::string>("cal_seq");
  config.cal_area = pt.get<std::string>("cal_area");
  config.n_const = pt.get<size_t>("n_const");
  config.top_tag_config = pt.get<std::string>("top_tag_config");
  for (const auto& cfg: pt.get_child("hbb_tag_config")) {
    config.hbb_tag_config.push_back(cfg.second.get_value<std::string>());
  }

  config.truth_wz_electron_container = pt.get<std::string>(
    "truth_wz_electron_container");

  cft::combine_files(pt.get_child("variables"),
                     fs::path(config_file_name));
  config.variables = cft::get_variable_list(pt.get_child("variables"));

  return config;
}
