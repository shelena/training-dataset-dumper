[![training-dataset-dumper docs](https://img.shields.io/badge/info-documentation-informational)][tddd]

Flavor Tagging Ntuple Dumper
============================

This is to dump b-tagging info from an AnalysisBase release.

The [training-dataset-dumper documentation][tddd] is hosted via mkdocs. In case of issues with the hosting, or to access older documentation, see the `docs/` directory in this package.

Quickstart
----------

If you want to get the code up and running fast [without reading][s] the documentation:

```bash
git clone ssh://git@gitlab.cern.ch:7999/atlas-flavor-tagging-tools/training-dataset-dumper.git
source training-dataset-dumper/setup-analysisbase.sh
mkdir build
cd build
cmake ../training-dataset-dumper
make
source build/x*/setup.sh

```

Run a test in `/tmp/` to see if everything worked:

```bash
test-dumper pflow
```

The first few lines printed to the terminal will tell you what code ran, and where. See the options in `test-dumper -h`. You can inspect the output files with

```bash
h5ls output.h5
```

[tddd]: https://training-dataset-dumper.docs.cern.ch/
[s]: https://www.youtube.com/watch?v=wooGSr7k1-s

